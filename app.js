/**
 * @file Main entry point for the backend of the application. Handles startup
 * and initilization tasks for modules, middleware, and database connections as
 * well as backend routes that are not handled by the frontend.
 * @author Ben Prisby <bdprisby@gmail.com>
 * @version 1.0.0
 */

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const db = require('./config/db'); // database config file
const environment = require('./config/environment'); // environment config file
const data = require('./routes/data'); // routes for data tasks
const users = require('./routes/users'); // routes for user tasks

const app = express();

// Set static folder for frontend build output
app.use(express.static(path.join(__dirname, 'public')));

// CORS middleware
app.use(cors());

// Body Parser middleware
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

// Map user routes to use /users/*
app.use('/users', users);

// Map data routes to use /data/*
app.use('/data', data);

// Index route
app.get('/', (req, res) => {
  res.send('Invalid Endpoint');
});

// Unknown page route (not handled by the frontend)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'static_assets/notfound.html'));
});

// Start the sever on the configured port
app.listen(environment.port, () => {
  console.log('Server started on port ' + environment.port);
});
