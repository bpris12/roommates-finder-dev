webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"container-fluid\">\n  <div class=\"container\">\n    <flash-messages></flash-messages>\n  </div>\n  <router-outlet (activate)=\"onActivate($event)\"></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/**
 * @file Angular main app component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent.prototype.onActivate = function (event) {
        window.scroll(0, 0);
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__ = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__ = __webpack_require__("./src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__ = __webpack_require__("./src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_dashboard_dashboard_component__ = __webpack_require__("./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_profile_profile_component__ = __webpack_require__("./src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_about_about_component__ = __webpack_require__("./src/app/components/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_getting_started_getting_started_component__ = __webpack_require__("./src/app/components/getting-started/getting-started.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_data_service__ = __webpack_require__("./src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__ = __webpack_require__("./src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_angular2_flash_messages__);
/**
 * @file Angular main app module.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















// Array of all frontend routes containing paths and corresponding components
var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a" /* RegisterComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_10__components_dashboard_dashboard_component__["a" /* DashboardComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_11__components_profile_profile_component__["a" /* ProfileComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_12__components_about_about_component__["a" /* AboutComponent */] },
    { path: 'getting-started', component: __WEBPACK_IMPORTED_MODULE_13__components_getting_started_getting_started_component__["a" /* GettingStartedComponent */] },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_about_about_component__["a" /* AboutComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_getting_started_getting_started_component__["a" /* GettingStartedComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_17_angular2_flash_messages__["FlashMessagesModule"].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_14__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_15__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/about/about.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h2 class=\"page-header\">About the App</h2>\n  <br>\n  <div class=\"row\">\n    <div class=\"col\">\n      <img src=\"../../../assets/images/students1.jpg\" alt=\"Students_1\"\n       style=\"width: 500px; border:1px solid #021a40; padding: 2px\">\n    </div>\n    <div class=\"col\">\n      <h4 class=\"display-5\">The Problem</h4>\n      <hr>\n      <p>\n        Finding roommates is a common problem for not only international\n        students but also domestic students. Since there are more than 250,000\n        college students in Boston, it is very difficult to have an organized\n        and legitimate process in place for finding future roommates. Even\n        though there are a couple of free roommate finder systems on the\n        internet, those are open for everyone and lack the information needed\n        in terms of tracking/filtering preferences while also maintaining\n        data privacy.\n      </p>\n    </div>\n  </div>\n  <br>\n  <div class=\"row\">\n    <div class=\"col\">\n      <h4 class=\"display-5\">The Solution</h4>\n      <hr>\n      <p>\n        The purpose of the Roommates Finder app is helping college students\n        (and non-students, for that matter) in Boston who want to find\n        potential roommates. This app aims to serve as a starting ground for\n        helping people find roommates that they will likely be most compatible\n        with by providing an easy-to-use interface that can be updated\n        continuously to reflect changes in demographics or preferences.\n      </p>\n    </div>\n    <div class=\"col\">\n      <img src=\"../../../assets/images/students2.jpg\" alt=\"Students_2\"\n      style=\"width: 500px; border:1px solid #021a40; padding: 2px\">\n    </div>\n  </div>\n</div>\n\n<div *ngIf=\"0\">\n<div class=\"carousel slide\" data-ride=\"carousel\" style=\"width: 450px\">\n  <div class=\"carousel-inner\">\n    <div class=\"carousel-item active\">\n      <img class=\"d-block w-100\" src=\"../../../assets/images/brownstones.jpg\"\n      alt=\"First slide\">\n    </div>\n    <div class=\"carousel-item\">\n      <img class=\"d-block w-100\" src=\"../../../assets/images/students1.jpg\"\n      alt=\"Second slide\">\n    </div>\n    <div class=\"carousel-item\">\n      <img class=\"d-block w-100\" src=\"../../../assets/images/students2.jpg\"\n      alt=\"Third slide\">\n    </div>\n  </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/**
 * @file Angular dashboard component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-about',
            template: __webpack_require__("./src/app/components/about/about.component.html"),
            styles: [__webpack_require__("./src/app/components/about/about.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\" class=\"container-fluid\">\n  <h2 class=\"page-header\">Dashboard</h2>\n  <p class=\"lead\">Welcome to your dashboard!</p>\n  <p>\n    Start looking for roommates below and use the buttons and checkboxes to\n    refine your results.\n  </p>\n  <hr>\n  <label>Popular searches based on your profile:</label><br>\n  <div class=\"button-wrapper\">\n    <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"lastPressedButton == 'all'\" (click)=\"onClickGetAllRoommates()\">All</button>\n    <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"lastPressedButton == 'school'\" [hidden]=\"user.school == undefined || user.school == 'No school selected...'\" (click)=\"onClickGetSchoolmates()\">People at {{user.school}}</button>\n    <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"lastPressedButton == 'year'\" [hidden]=\"user.yearInSchool == undefined\" (click)=\"onClickGetSameYears()\">{{user.yearInSchool}} Year Students</button>\n    <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"lastPressedButton == 'major'\" [hidden]=\"user.major == undefined\" (click)=\"onClickGetSameMajors()\">{{user.major}} Majors</button>\n    <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"lastPressedButton == 'location'\" [hidden]=\"user.preferenceid == undefined || preferences.firstChoiceNeighborhood == 'No neighborhood selected...'\" (click)=\"onClickGetSameLocation()\">Also Looking in {{preferences.firstChoiceNeighborhood}}</button>\n    <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"lastPressedButton == 'budget'\" [hidden]=\"user.preferenceid == undefined\" (click)=\"onClickGetSimilarBudgets()\">People with Similar Budgets</button>\n  </div>\n  <hr>\n  <label>Other filters:</label><br>\n  <div class=\"row\">\n    <div class=\"col-2\">\n      <div class=\"form-check\">\n        <input type=\"checkbox\" name=\"petCheck_input\" class=\"form-check-input\" [(ngModel)]=\"petCheck\" #petCheck_input=\"ngModel\" (change)=\"onClickPreferPets()\">\n        <label class=\"form-check-label\">Prefers pets</label>\n      </div>\n    </div>\n    <div class=\"col-4\">\n      <div class=\"form-check\">\n        <input type=\"checkbox\" name=\"utilitiesCheck_input\" class=\"form-check-input\" [(ngModel)]=\"utilitiesCheck\" #utilitiesCheck_input=\"ngModel\" (change)=\"onClickPreferUtilities()\">\n        <label class=\"form-check-label\">Wants utilities included</label>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-2\" *ngIf=\"petCheck\">\n      <br>\n      <button type=\"button\" class=\"btn btn-warning btn-sm\" (click)=\"onClickClearPets()\">Clear pets filter</button>\n    </div>\n    <div class=\"col-2\" *ngIf=\"utilitiesCheck\">\n      <br>\n      <button type=\"button\" class=\"btn btn-warning btn-sm\" (click)=\"onClickClearUtilities()\">Clear utilities filter</button>\n    </div>\n  </div>\n  <hr>\n  <h4>Roommates:</h4>\n  <div class=\"table-responsive\">\n    <table class=\"table table-hover table-bordered\">\n    <thead class=\"thead-dark\">\n      <tr>\n        <th scope=\"col\">Name</th>\n        <th scope=\"col\">School</th>\n        <th scope=\"col\">Year in School</th>\n        <th scope=\"col\">Major</th>\n        <th scope=\"col\">Preferred Location</th>\n        <th scope=\"col\">Partying</th>\n        <th scope=\"col\">Sleep</th>\n        <th scope=\"col\">Noise</th>\n        <th scope=\"col\"># of People</th>\n        <th scope=\"col\"></th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let roommate of roommates\">\n        <td>\n          {{roommate.fullName}}\n          <em *ngIf=\"roommate.userid == user.userid\">(me)</em>\n        </td>\n        <td>\n          <div *ngIf=\"roommate.school != 'No school selected...'\">\n            {{roommate.school}}\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"roommate.school != 'No school selected...'\">\n            {{roommate.yearInSchool}}\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"roommate.school != 'No school selected...'\">\n            {{roommate.major}}\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"roommate.preferenceid != undefined && roommate.firstChoiceNeighborhood != 'No neighborhood selected...'\">\n            {{roommate.firstChoiceNeighborhood}}\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"user.preferenceid == undefined || roommate.preferenceid == undefined && user.userid != roommate.userid\">\n            N/A\n          </div>\n          <div *ngIf=\"user.preferenceid != undefined && roommate.preferenceid != undefined && user.userid != roommate.userid\">\n            <div *ngIf=\"Math.abs(preferences.party - roommate.party) <= 1\">\n              <img src=\"../../../assets/images/green-smiley.png\" alt=\"green_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.party - roommate.party) > 1 && Math.abs(preferences.party - roommate.party) <= 3\">\n              <img src=\"../../../assets/images/yellow-smiley.png\" alt=\"yellow_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.party - roommate.party) > 3\">\n              <img src=\"../../../assets/images/red-smiley.png\" alt=\"red_smiley\" width=\"20px\">\n            </div>\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"user.preferenceid == undefined || roommate.preferenceid == undefined && user.userid != roommate.userid\">\n            N/A\n          </div>\n          <div *ngIf=\"user.preferenceid != undefined && roommate.preferenceid != undefined && user.userid != roommate.userid\">\n            <div *ngIf=\"Math.abs(preferences.sleep - roommate.sleep) <= 1\">\n              <img src=\"../../../assets/images/green-smiley.png\" alt=\"green_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.sleep - roommate.sleep) > 1 && Math.abs(preferences.sleep - roommate.sleep) <= 3\">\n              <img src=\"../../../assets/images/yellow-smiley.png\" alt=\"yellow_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.sleep - roommate.sleep) > 3\">\n              <img src=\"../../../assets/images/red-smiley.png\" alt=\"red_smiley\" width=\"20px\">\n            </div>\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"user.preferenceid == undefined || roommate.preferenceid == undefined && user.userid != roommate.userid\">\n            N/A\n          </div>\n          <div *ngIf=\"user.preferenceid != undefined && roommate.preferenceid != undefined && user.userid != roommate.userid\">\n            <div *ngIf=\"Math.abs(preferences.noiseLevel - roommate.noiseLevel) <= 1\">\n              <img src=\"../../../assets/images/green-smiley.png\" alt=\"green_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.noiseLevel - roommate.noiseLevel) > 1 && Math.abs(preferences.noiseLevel - roommate.noiseLevel) <= 3\">\n              <img src=\"../../../assets/images/yellow-smiley.png\" alt=\"yellow_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.noiseLevel - roommate.noiseLevel) > 3\">\n              <img src=\"../../../assets/images/red-smiley.png\" alt=\"red_smiley\" width=\"20px\">\n            </div>\n          </div>\n        </td>\n        <td>\n          <div *ngIf=\"user.preferenceid == undefined || roommate.preferenceid == undefined && user.userid != roommate.userid\">\n            N/A\n          </div>\n          <div *ngIf=\"user.preferenceid != undefined && roommate.preferenceid != undefined && user.userid != roommate.userid\">\n            <div *ngIf=\"Math.abs(preferences.maxNumofRoommates - roommate.maxNumofRoommates) <= 1\">\n              <img src=\"../../../assets/images/green-smiley.png\" alt=\"green_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.maxNumofRoommates - roommate.maxNumofRoommates) > 1 && Math.abs(preferences.maxNumofRoommates - roommate.maxNumofRoommates) <= 3\">\n              <img src=\"../../../assets/images/yellow-smiley.png\" alt=\"yellow_smiley\" width=\"20px\">\n            </div>\n            <div *ngIf=\"Math.abs(preferences.maxNumofRoommates - roommate.maxNumofRoommates) > 3\">\n              <img src=\"../../../assets/images/red-smiley.png\" alt=\"red_smiley\" width=\"20px\">\n            </div>\n          </div>\n        </td>\n        <td style=\"width:4%\">\n          <a *ngIf=\"roommate.userid != user.userid\" href=\"mailto:{{roommate.email}}\">\n            <img src=\"../../../assets/images/email.svg\" alt=\"email-glyphicon\" width=\"22px\">\n          </a>\n        </td>\n      </tr>\n    </tbody>\n  </table>\n\n  <table class=\"table table-borderless\">\n    <tbody>\n      <tr>\n        <td style=\"text-align:center\">\n          <img src=\"../../../assets/images/green-smiley.png\" alt=\"green_smiley\" width=\"20px\">\n          <small>= You feel very almost the same way!</small>\n        </td>\n        <td style=\"text-align:center\">\n          <img src=\"../../../assets/images/yellow-smiley.png\" alt=\"yellow_smiley\" width=\"20px\">\n          <small>= You feel sort of the same way</small>\n        </td>\n        <td style=\"text-align:center\">\n          <img src=\"../../../assets/images/red-smiley.png\" alt=\"red_smiley\" width=\"20px\">\n          <small>= You're two different people about this!</small>\n        </td>\n      </tr>\n    </tbody>\n  </table>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/**
 * @file Angular dashboard component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(authService) {
        this.authService = authService;
        this.Math = Math; // Allows for use within HTML
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Grab the current user information
        this.authService.getProfile().subscribe(function (profile) {
            _this.user = profile.user.user;
            _this.preferences = profile.user.preferences;
        }, function (err) {
            console.log(err);
            return false;
        });
        // Start by getting everyone in the database
        this.onClickGetAllRoommates();
        // Set these to false to initalize, but they have data binding to update
        this.petCheck = false;
        this.utilitiesCheck = false;
    };
    // When the user clicks the "All" button
    DashboardComponent.prototype.onClickGetAllRoommates = function () {
        var _this = this;
        // Get all of the roommates from the database
        this.authService.getAllRoommates().subscribe(function (results) {
            _this.roommates = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.lastPressedButton = 'all'; // Keep track of the last button press
    };
    // When the user clicks the "People at [school]" button
    DashboardComponent.prototype.onClickGetSchoolmates = function () {
        var _this = this;
        this.authService.getSchoolmates().subscribe(function (results) {
            _this.roommates = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.lastPressedButton = 'school';
    };
    // When the user clicks the "[major] Majors" button
    DashboardComponent.prototype.onClickGetSameMajors = function () {
        var _this = this;
        this.authService.getSameMajors().subscribe(function (results) {
            _this.roommates = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.lastPressedButton = 'major';
    };
    // When the user clicks the "[year] Year Students" button
    DashboardComponent.prototype.onClickGetSameYears = function () {
        var _this = this;
        this.authService.getSameYears().subscribe(function (results) {
            _this.roommates = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.lastPressedButton = 'year';
    };
    // When the user clicks the "Also Looking in [location]" button
    DashboardComponent.prototype.onClickGetSameLocation = function () {
        var _this = this;
        this.authService.getSameLocation().subscribe(function (results) {
            _this.roommates = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.lastPressedButton = 'location';
    };
    // When the user clicks the "People with Similar Budgets" button
    DashboardComponent.prototype.onClickGetSimilarBudgets = function () {
        var _this = this;
        this.authService.getSimilarBudgets().subscribe(function (results) {
            _this.roommates = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        this.lastPressedButton = 'budget';
    };
    // When the "Prefers pets" checkbox is changed (check/uncheck)
    // NOTE: We need to be mindful of the state of the other checkbox to ensure
    //       everything displays as expected, hence why that state is also passed
    //       into the service function
    DashboardComponent.prototype.onClickPreferPets = function () {
        var _this = this;
        // Check the state of the checkbox
        if (this.petCheck) {
            this.authService.getPreferPets(this.lastPressedButton, this.utilitiesCheck).subscribe(function (results) {
                _this.roommates = results;
            }, function (err) {
                console.log(err);
                return false;
            });
        }
        else {
            this.onClickClearPets();
        }
    };
    // Run when the "Prefers pets" checkbox is unchecked or the
    // "Clear pets filter" button is clicked
    DashboardComponent.prototype.onClickClearPets = function () {
        this.petCheck = false; // In case the button was pressed
        // Check if the utilities box is checked
        if (this.utilitiesCheck) {
            this.onClickPreferUtilities(); // This will take care of everything
        }
        else {
            // Check what the active filter button is and rerun that query
            switch (this.lastPressedButton) {
                case 'all':
                    this.onClickGetAllRoommates();
                    break;
                case 'school':
                    this.onClickGetSchoolmates();
                    break;
                case 'major':
                    this.onClickGetSameMajors();
                    break;
                case 'year':
                    this.onClickGetSameYears();
                    break;
                case 'location':
                    this.onClickGetSameLocation();
                    break;
                case 'budget':
                    this.onClickGetSimilarBudgets();
                    break;
            }
        }
    };
    // When the "Wants utilities included" checkbox is changed (check/uncheck)
    DashboardComponent.prototype.onClickPreferUtilities = function () {
        var _this = this;
        if (this.utilitiesCheck) {
            this.authService.getPreferUtilities(this.lastPressedButton, this.petCheck).subscribe(function (results) {
                _this.roommates = results;
            }, function (err) {
                console.log(err);
                return false;
            });
        }
        else {
            this.onClickClearUtilities();
        }
    };
    DashboardComponent.prototype.onClickClearUtilities = function () {
        this.utilitiesCheck = false;
        // This time, consult the "Prefers pets" checkbox
        if (this.petCheck) {
            this.onClickPreferPets();
        }
        else {
            switch (this.lastPressedButton) {
                case 'all':
                    this.onClickGetAllRoommates();
                    break;
                case 'school':
                    this.onClickGetSchoolmates();
                    break;
                case 'major':
                    this.onClickGetSameMajors();
                    break;
                case 'year':
                    this.onClickGetSameYears();
                    break;
                case 'location':
                    this.onClickGetSameLocation();
                    break;
                case 'budget':
                    this.onClickGetSimilarBudgets();
                    break;
            }
        }
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("./src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("./src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/components/getting-started/getting-started.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/getting-started/getting-started.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h2 class=\"page-header\">Getting Started</h2>\n  <br>\n  <h4 class=\"display-5\">Overview</h4>\n  <hr>\n  <p>\n    It's easy to get up and running with this app. Take a look\n    below for more information about the functionality of each of the pages.\n  </p>\n  <br>\n  <h4 class=\"display-5\">Using the App</h4>\n  <hr>\n  <div class=\"row\">\n    <div class=\"col\">\n      <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">\n        <div class=\"card-header\">Creating your account</div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">Register</h5>\n          <p class=\"card-text\">\n            Allows you to get your account created in the database. Simply fill\n            out the required information (and optional stuff if you choose) to\n            get started!\n          </p>\n          <button class=\"btn btn-primary\" [disabled]=\"authService.loggedIn()\" [routerLink]=\"['/register']\">Go to Register</button>\n        </div>\n      </div>\n    </div>\n    <div class=\"col\">\n      <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">\n        <div class=\"card-header\">Logging into your account</div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">Login</h5>\n          <p class=\"card-text\">\n            After registering for your new account, head over to the login page\n            to sign in and gain access to the inner workings of the app!\n          </p>\n          <button class=\"btn btn-primary\" [disabled]=\"authService.loggedIn()\" [routerLink]=\"['/login']\">Go to Login</button>\n        </div>\n      </div>\n    </div>\n    <div class=\"col\">\n      <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">\n        <div class=\"card-header\">Editing your profile</div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">Profile</h5>\n          <p class=\"card-text\">\n            Use this page to edit any personal information that might be subject\n            to change. Here, you'll also be able to manage and tweak your\n            preferences for use in the roommate search functionality.\n          </p>\n          <button class=\"btn btn-primary\" [disabled]=\"!authService.loggedIn()\" [routerLink]=\"['/profile']\">Go to Profile</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-1\"></div>\n    <div class=\"col\">\n      <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">\n        <div class=\"card-header\">Searching for roommates</div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">Dashboard</h5>\n          <p class=\"card-text\">\n            This page is the heart of the whole app. You'll be able to see all\n            of the other roommates in the database, filter results to get only\n            people more relevant to you, and see compatibility indicators\n            derived from comparing each of your preferences.\n          </p>\n          <button class=\"btn btn-primary\" [disabled]=\"!authService.loggedIn()\" [routerLink]=\"['/dashboard']\">Go to Dashboard</button>\n        </div>\n      </div>\n    </div>\n    <div class=\"col\">\n      <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">\n        <div class=\"card-header\">Learning more about the app</div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">About</h5>\n          <p class=\"card-text\">\n            If you're curious to know more about the problem that spurred the\n            creation of this app, check out this page. Or, you might not care\n            and that's okay too!\n          </p>\n          <button class=\"btn btn-primary\" [routerLink]=\"['/about']\">Go to About</button>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-1\"></div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/getting-started/getting-started.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GettingStartedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/**
 * @file Angular getting started component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GettingStartedComponent = /** @class */ (function () {
    function GettingStartedComponent(authService) {
        this.authService = authService;
    }
    GettingStartedComponent.prototype.ngOnInit = function () { };
    GettingStartedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-getting-started',
            template: __webpack_require__("./src/app/components/getting-started/getting-started.component.html"),
            styles: [__webpack_require__("./src/app/components/getting-started/getting-started.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]])
    ], GettingStartedComponent);
    return GettingStartedComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"carousel slide\" data-ride=\"carousel\">\n    <div class=\"carousel-inner\">\n      <div class=\"carousel-item active\">\n        <img class=\"d-block w-100\" src=\"../../../assets/images/roommates3.jpg\">\n      </div>\n      <div class=\"carousel-item\">\n        <img class=\"d-block w-100\" src=\"../../../assets/images/roommates1.jpg\">\n      </div>\n      <div class=\"carousel-item\">\n        <img class=\"d-block w-100\" src=\"../../../assets/images/roommates4.jpg\">\n      </div>\n      <div class=\"carousel-item\">\n        <img class=\"d-block w-100\" src=\"../../../assets/images/roommates2.jpg\">\n      </div>\n      <div class=\"carousel-item\">\n        <img class=\"d-block w-100\" src=\"../../../assets/images/roommates5.jpg\">\n      </div>\n    </div>\n  </div>\n  <div class=\"jumbotron text-center\">\n    <h1>Roommates Finder</h1>\n    <p class=\"lead\"> Welcome to a new way of finding potential roommates around Boston, built from scratch!</p>\n    <div>\n      <a class=\"btn btn-primary\" *ngIf=\"!authService.loggedIn()\" [routerLink]=\"['/register']\">Register</a>\n      <a class=\"btn btn-secondary\" *ngIf=\"!authService.loggedIn()\" [routerLink]=\"['/login']\">Login</a>\n      <a class=\"btn btn-primary\" *ngIf=\"authService.loggedIn()\" [routerLink]=\"['/dashboard']\">Dashboard</a>\n      <a class=\"btn btn-secondary\" *ngIf=\"authService.loggedIn()\" [routerLink]=\"['/profile']\">Profile</a>\n    </div>\n  </div>\n  <br>\n  <div class=\"row\">\n    <div class=\"col-md-4\">\n      <h3>Node.js Backend</h3>\n      <hr>\n      <img src=\"../../../assets/images/node.png\" alt=\"Node_Logo\">\n      <br><br>\n      <p>Robust and secure Node.js backend server running the Express.js web framework for easy routing and middleware integration.</p>\n    </div>\n    <div class=\"col-md-4\">\n      <h3>Angular 5 Frontend</h3>\n      <hr>\n      <img src=\"../../../assets/images/angular.png\" alt=\"Angular_Logo\">\n      <br><br>\n      <p>Powerful Angular frontend for serving up an intuitive, flexible UI to make and manage requests to the backend server.</p>\n    </div>\n    <div class=\"col-md-4\">\n      <h3>MySQL Database</h3>\n      <hr>\n      <img src=\"../../../assets/images/mysql.png\" alt=\"MySQL_Logo\">\n      <br><br>\n      <p>Data schema implemented in MySQL and powered by the Node.js connector for interacting with the database.</p>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/**
 * @file Angular home component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(authService) {
        this.authService = authService;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("./src/app/components/home/home.component.html"),
            styles: [__webpack_require__("./src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h2 class=\"page-header\">Login</h2>\n  <form (submit)=\"onLoginSubmit()\">\n    <div class=\"form-group\">\n      <label>Email</label>\n      <input type=\"email\" name=\"email_input\" pattern=\"^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$\" [ngClass]=\"(email_input.invalid && (email_input.dirty || email_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"email\" #email_input=\"ngModel\" placeholder=\"name@example.com\">\n      <div *ngIf=\"email_input.invalid && (email_input.dirty || email_input.touched)\" class=\"invalid-feedback\">\n        <div *ngIf=\"email_input.errors.required\">\n          Email is required.\n        </div>\n        <div *ngIf=\"email_input.errors.pattern\">\n          Please enter a valid email.\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label>Password</label>\n      <input type=\"password\" name=\"password_input\" [ngClass]=\"(password_input.invalid && (password_input.dirty || password_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"password\" #password_input=\"ngModel\">\n      <div *ngIf=\"password_input.invalid && (password_input.dirty || password_input.touched)\" class=\"invalid-feedback\">\n        <div *ngIf=\"password_input.errors.required\">\n          Password is required.\n        </div>\n      </div>\n    </div>\n    <button type=\"submit\" class=\"btn btn-primary\">Login</button>\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/**
 * @file Angular login component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, authService, flashMessage) {
        this.router = router;
        this.authService = authService;
        this.flashMessage = flashMessage;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    // Login procedure started by clicking the "Login" button
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        // Construct a partial user object to send with the POST request
        var user = {
            email: this.email,
            password: this.password
        };
        // Authenticate the user
        this.authService.authenticateUser(user).subscribe(function (data) {
            // If successful, store the token and user object locally, show a
            // successful flash message, and go to the profile page
            if (data.success) {
                _this.authService.storeUserData(data.token);
                _this.flashMessage.show('You are now logged in.', { cssClass: 'alert-success', timeout: 4000 });
                _this.router.navigate(['/profile']);
            }
            else {
                _this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
                _this.router.navigate(['/login']);
            }
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/components/login/login.component.html"),
            styles: [__webpack_require__("./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\">\n  <a class=\"navbar-brand\" [routerLink]=\"['/']\">Roommates Finder</a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n  <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item active\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" [routerLink]=\"['/']\">Home</a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" [routerLink]=\"['/about']\">About</a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" [routerLink]=\"['/getting-started']\">Getting Started</a>\n      </li>\n    </ul>\n\n    <ul class=\"navbar-nav ml-auto\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" *ngIf=\"authService.loggedIn()\">\n        <a class=\"nav-link\" [routerLink]=\"['/dashboard']\">Dashboard</a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" *ngIf=\"authService.loggedIn()\">\n        <a class=\"nav-link\" [routerLink]=\"['/profile']\">Profile</a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"!authService.loggedIn()\">\n        <a class=\"nav-link\" [routerLink]=\"['/login']\">Login</a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\" *ngIf=\"!authService.loggedIn()\">\n        <a class=\"nav-link\" [routerLink]=\"['/register']\">Register</a>\n      </li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\">\n        <a class=\"nav-link\" (click)=\"onLogoutClick()\" href=\"#\">Logout</a>\n      </li>\n    </ul>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/**
 * @file Angular navbar component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = /** @class */ (function () {
    // Inject the necessary services
    function NavbarComponent(router, authService, flashMessage) {
        this.router = router;
        this.authService = authService;
        this.flashMessage = flashMessage;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    // Logout procedure when the user clicks "Logout" on the navbar
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.show('You are logged out.', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/login']);
        return false;
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("./src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/profile/profile.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\" class=\"container\">\n  <h2 class=\"display-4\">{{user.fullName}}</h2>\n  <p class=\"lead\">User ID: {{user.userid}}</p>\n  <div *ngIf=\"user.preferenceid == undefined\" class=\"alert alert-danger\" role=\"alert\">\n    NOTE: You have not filled out your preferences. Please do so to take full advantage of this app!\n  </div>\n  <ul class=\"list-group\">\n    <li class=\"list-group-item\">\n      <span class=\"align-middle\">Email: <strong>{{user.email}}</strong></span>\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#emailModal\" (click)=\"onClickEdit()\">Edit</button>\n      </div>\n    </li>\n    <li class=\"list-group-item\">\n      <span class=\"align-middle\">Gender: <strong>{{user.gender}}</strong></span>\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#genderModal\" (click)=\"onClickEdit()\">Edit</button>\n      </div>\n    </li>\n    <li class=\"list-group-item\">\n      <span class=\"align-middle\">Relationship Status: <strong>{{user.relationshipStatus}}</strong></span>\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#relationshipModal\" (click)=\"onClickEdit()\">Edit</button>\n      </div>\n    </li>\n    <li class=\"list-group-item\">\n      <span class=\"align-middle\">School: <strong>{{user.school}}</strong></span>\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#schoolModal\" (click)=\"onClickEdit()\">Edit</button>\n      </div>\n    </li>\n    <li *ngIf=\"user.school != 'No school selected...'\" class=\"list-group-item\">\n      <span class=\"align-middle\">Year in School: <strong>{{user.yearInSchool}}</strong></span>\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#yearModal\" (click)=\"onClickEdit()\">Edit</button>\n      </div>\n    </li>\n    <li *ngIf=\"user.school != 'No school selected...'\" class=\"list-group-item\">\n      <span class=\"align-middle\">Major: <strong>{{user.major}}</strong></span>\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#majorModal\" (click)=\"onClickEdit()\">Edit</button>\n      </div>\n    </li>\n  </ul>\n  <br>\n  <h4 class=\"display-5\">Preferences</h4>\n  <div *ngIf=\"user.preferenceid == undefined\">\n    <p>You do not have a preferences profile. Click the button below to create one!</p>\n    <button type=\"button\" class=\"btn btn-primary\" (click)=\"initPreferences()\">Create Preferences Profile</button>\n    <hr>\n  </div>\n  <div *ngIf=\"user.preferenceid != undefined\">\n    <p>Adjust the following settings to represent your personal preferences. These will be used to help show you more relevant searches.</p>\n    <hr>\n    <div class=\"row\">\n      <div class=\"col\">\n        <label>Partying:</label>\n        <div class=\"float-right\">\n          <small>Hate it!</small>\n        </div>\n      </div>\n      <div class=\"col-7\">\n        <div class=\"slidecontainer\">\n          <input type=\"range\" name=\"party_input\" min=\"0\" max=\"10\" class=\"slider\" [(ngModel)]=\"preferences.party\" #party_input=\"ngModel\" (change)=\"onChangeTouchSlider()\">\n        </div>\n      </div>\n      <div class=\"col\">\n        <div class=\"float-left\">\n          <small>Love it!</small>\n        </div>\n        <div class=\"float-right\">\n          <strong>{{preferences.party}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col\">\n        <label>Sleeping:</label>\n        <div class=\"float-right\">\n          <small>Early bird!</small>\n        </div>\n      </div>\n      <div class=\"col-7\">\n        <div class=\"slidecontainer\">\n          <input type=\"range\" name=\"sleep_input\" min=\"0\" max=\"10\" class=\"slider\" [(ngModel)]=\"preferences.sleep\" #sleep_input=\"ngModel\" (change)=\"onChangeTouchSlider()\">\n        </div>\n      </div>\n      <div class=\"col\">\n        <div class=\"float-left\">\n          <small>Night owl!</small>\n        </div>\n        <div class=\"float-right\">\n          <strong>{{preferences.sleep}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col\">\n        <label>Noise Level:</label>\n        <div class=\"float-right\">\n          <small>Quiet!</small>\n        </div>\n      </div>\n      <div class=\"col-7\">\n        <div class=\"slidecontainer\">\n          <input type=\"range\" name=\"noise_input\" min=\"0\" max=\"10\" class=\"slider\" [(ngModel)]=\"preferences.noiseLevel\" #noise_input=\"ngModel\" (change)=\"onChangeTouchSlider()\">\n        </div>\n      </div>\n      <div class=\"col\">\n        <div class=\"float-left\">\n          <small>Loud!</small>\n        </div>\n        <div class=\"float-right\">\n          <strong>{{preferences.noiseLevel}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col\">\n        <label>Max # of Roommates:</label>\n      </div>\n      <div class=\"col-7\">\n        <div class=\"slidecontainer\">\n          <input type=\"range\" name=\"maxNum_input\" min=\"1\" max=\"8\" class=\"slider\" [(ngModel)]=\"preferences.maxNumofRoommates\" #maxNum_input=\"ngModel\" (change)=\"onChangeTouchSlider()\">\n        </div>\n      </div>\n      <div class=\"col\">\n        <div class=\"float-right\">\n          <strong>{{preferences.maxNumofRoommates}}</strong>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"row\">\n      <div class=\"col\">\n        <span class=\"align-middle\">\n          <label>Pet Preference:</label>\n        </span>\n      </div>\n      <div class=\"col-7\">\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"preferences.petPreference\" name=\"petPreference\" [value]=\"1\" (click)=\"onChangeTouchSlider()\">\n          <label class=\"form-check-label\">Pets are okay with me!</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"preferences.petPreference\" name=\"petPreference\" [value]=\"0\" (click)=\"onChangeTouchSlider()\">\n          <label class=\"form-check-label\">No pets, please!</label>\n        </div>\n      </div>\n      <div class=\"col\"></div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col\">\n        <span class=\"align-middle\">\n          <label>Utilities Preference:</label>\n        </span>\n      </div>\n      <div class=\"col-7\">\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"preferences.utilitiesIncluded\" name=\"utilitiesIncluded\" [value]=\"1\" (click)=\"onChangeTouchSlider()\">\n          <label class=\"form-check-label\">Utilities included</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"preferences.utilitiesIncluded\" name=\"utilitiesIncluded\" [value]=\"0\" (click)=\"onChangeTouchSlider()\">\n          <label class=\"form-check-label\">Don't care</label>\n        </div>\n      </div>\n      <div class=\"col\"></div>\n    </div>\n    <hr>\n    <div class=\"row\">\n      <div class=\"col\">\n        <span class=\"align-middle\">\n          <label>First Choice Neighborhood:</label>\n        </span>\n      </div>\n      <div class=\"col-8\">\n        <div class=\"form-group\">\n          <select class=\"form-control\" [(ngModel)]=\"preferences.firstChoiceNeighborhood\" name=\"neighborhood_input\" (change)=\"onChangeTouchSlider()\">\n            <option *ngFor=\"let neighborhood of neighborhoods\" [value]=\"neighborhood.neighborhoodName\">\n              {{neighborhood.neighborhoodName}}\n            </option>\n          </select>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"row\">\n      <div class=\"col\">\n        <span class=\"align-middle\">\n          <label>Min Budget:</label>\n        </span>\n      </div>\n      <div class=\"col-8\">\n        <div class=\"input-group mb-3\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\">$</span>\n          </div>\n          <input type=\"text\" name=\"minBudget_input\" pattern= \"[0-9]{1,4}\" [ngClass]=\"minBudget_input.invalid ? 'form-control is-invalid' : 'form-control'\" aria-label=\"Amount (to the nearest dollar)\" required [(ngModel)]=\"preferences.minBudget\" #minBudget_input=\"ngModel\" (change)=\"onChangeTouchSlider()\">\n          <div class=\"input-group-append\">\n            <span class=\"input-group-text\">.00</span>\n          </div>\n          <div *ngIf=\"minBudget_input.invalid\" class=\"invalid-feedback\">\n            Please enter a valid number between 1-4 digits\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col\">\n        <span class=\"align-middle\">\n          <label>Max Budget:</label>\n        </span>\n      </div>\n      <div class=\"col-8\">\n        <div class=\"input-group mb-3\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\">$</span>\n          </div>\n          <input type=\"text\" name=\"maxBudget_input\" pattern= \"[0-9]{1,4}\" [ngClass]=\"maxBudget_input.invalid ? 'form-control is-invalid' : 'form-control'\" aria-label=\"Amount (to the nearest dollar)\" required [(ngModel)]=\"preferences.maxBudget\" #maxBudget_input=\"ngModel\" (change)=\"onChangeTouchSlider()\">\n          <div class=\"input-group-append\">\n            <span class=\"input-group-text\">.00</span>\n          </div>\n          <div *ngIf=\"maxBudget_input.invalid\" class=\"invalid-feedback\">\n            Please enter a valid number between 1-4 digits\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div *ngIf=\"touchedSliders && !minBudget_input.invalid && !maxBudget_input.invalid\">\n      <hr>\n      <button class=\"btn btn-warning btn-block\" (click)=\"onClickUpdatePreferences()\">Update Preferences</button>\n    </div>\n  </div>\n  <hr>\n  <button class=\"btn btn-danger btn-block\" data-toggle=\"modal\" data-target=\"#deleteModal\">Delete Profile</button>\n  <br>\n\n  <div class=\"modal fade\" id=\"emailModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"emailModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"emailModalLabel\">Update Email</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label class=\"control-label\">Email</label>\n            <div class=\"input-group\">\n              <input type=\"email\" name=\"email_input\" pattern=\"^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$\" [ngClass]=\"(email_input.invalid && (email_input.dirty || email_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"updatedUser.email\" #email_input=\"ngModel\">\n              <div *ngIf=\"email_input.invalid && (email_input.dirty || email_input.touched)\" class=\"invalid-feedback\">\n                <div *ngIf=\"email_input.errors.required\">\n                  Email is required.\n                </div>\n                <div *ngIf=\"email_input.errors.pattern\">\n                  Please enter a valid email.\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" [disabled]=\"email_input.invalid || (!email_input.dirty)\" (click)=\"onClickUpdateEmail()\">Save changes</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"genderModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"genderModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"genderModalLabel\">Update Gender</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label class=\"control-label\">Gender</label>\n            <select class=\"form-control\" [(ngModel)]=\"updatedUser.gender\" name=\"gender_input\">\n              <option>Male</option>\n              <option>Female</option>\n              <option>Other</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"onClickUpdateGender()\">Save Changes</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"relationshipModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"relationshipModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"relationshipModalLabel\">Update Relationship Status</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label class=\"control-label\">Relationship Status</label>\n            <select class=\"form-control\" [(ngModel)]=\"updatedUser.relationshipStatus\" name=\"relationship_input\">\n              <option>Single</option>\n              <option>In a relationship</option>\n              <option>Engaged</option>\n              <option>Married</option>\n              <option>Widowed</option>\n              <option>Separated</option>\n              <option>Divorced</option>\n              <option>In a civil union</option>\n              <option>In a domestic partnership</option>\n              <option>Prefer not to say</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"onClickUpdateRelationshipStatus()\">Save Changes</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"schoolModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"schoolModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"schoolModalLabel\">Update School</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label class=\"control-label\">School</label>\n            <select class=\"form-control\" [(ngModel)]=\"updatedUser.school\" name=\"school_input\">\n              <option *ngFor=\"let school of schools\" [value]=\"school.schoolName\">\n                {{school.schoolName}}\n              </option>\n            </select>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"onClickUpdateSchool()\">Save Changes</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"yearModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"yearModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"yearModalLabel\">Update Year in School</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label class=\"control-label\">Year in School</label>\n            <select class=\"form-control\" [(ngModel)]=\"updatedUser.yearInSchool\" name=\"year_input\">\n              <option>First</option>\n              <option>Second</option>\n              <option>Third</option>\n              <option>Fourth</option>\n              <option>Fifth</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"onClickUpdateYearInSchool()\">Save Changes</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"majorModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"majorModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"majorModalLabel\">Update Major</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label class=\"control-label\">Major</label>\n            <input type=\"text\" name=\"major_input\" class=\"form-control\" [(ngModel)]=\"updatedUser.major\" #major_input=\"ngModel\">\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" (click)=\"onClickUpdateMajor()\">Save Changes</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"deleteModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"deleteModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"deleteModalLabel\">Delete Profile</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          Are you sure you want to delete your profile? This action cannot be undone.\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\n          <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\" (click)=\"onClickDelete()\">Delete</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("./src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_preference__ = __webpack_require__("./src/app/models/preference.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_user__ = __webpack_require__("./src/app/models/user.ts");
/**
 * @file Angular profile component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router, authService, dataService, flashMessage) {
        this.router = router;
        this.authService = authService;
        this.dataService = dataService;
        this.flashMessage = flashMessage;
        // Initialize the updated user with dummy values
        // NOTE: This is okay, as we will only be polling for values of interest,
        //       not the whole object, when we try to propagate changes
        this.updatedUser = new __WEBPACK_IMPORTED_MODULE_6__models_user__["a" /* User */]();
        this.updatedUser.fillInPlaceholders();
        this.touchedSliders = false;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Grab the user's information
        this.loadProfileInfo();
        // Grab all of the schools from the DB
        this.dataService.getAllSchools().subscribe(function (results) {
            _this.schools = results;
        }, function (err) {
            console.log(err);
            return false;
        });
        // Grab all of the neighborhoods from the DB
        this.dataService.getAllNeighborhoods().subscribe(function (results) {
            _this.neighborhoods = results;
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    // Get the profile information to be displayed on the page
    ProfileComponent.prototype.loadProfileInfo = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            // NOTE: "user" is just a name from the Passport packet coming from the
            //       backend, hence why this looks a little strange...
            _this.user = profile.user.user;
            _this.preferences = profile.user.preferences; // These are included too
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    // Called when the user does not have any linked preferences and needs them
    ProfileComponent.prototype.initPreferences = function () {
        var _this = this;
        // Create a holder object for new preferences
        var blankPrefs = new __WEBPACK_IMPORTED_MODULE_5__models_preference__["a" /* Preference */]();
        // Fill in default values to start - the user will adjust them
        blankPrefs.initDefaults();
        // Send the default object to the backend to create a primary key and
        // apply the foreign key constraint to the user profile
        this.authService.createPreferences(this.user, blankPrefs).subscribe(function (data) {
            if (data.success) {
                _this.flashMessage.show('Your preferences profile has been created!', { cssClass: 'alert-success', timeout: 3000 });
                _this.loadProfileInfo();
            }
            else {
                _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                _this.router.navigate(['/profile']);
            }
        });
    };
    // Function for controlling the show/hide of certain HTML buttons
    ProfileComponent.prototype.onChangeTouchSlider = function () {
        this.touchedSliders = true;
        // Do a little data validation on the budget values
        if ((this.preferences.minBudget == undefined) || (this.preferences.minBudget < 0)) {
            this.preferences.minBudget = 0;
        }
        if ((this.preferences.maxBudget == undefined) || (this.preferences.maxBudget > 9999)) {
            this.preferences.maxBudget = 9999;
        }
    };
    // Copy the user info into a new object for manipulation
    ProfileComponent.prototype.onClickEdit = function () {
        this.updatedUser = JSON.parse(JSON.stringify(this.user)); // Workaround
    };
    // Run when the user clicks the "Save changes" button while editing email
    ProfileComponent.prototype.onClickUpdateEmail = function () {
        var _this = this;
        // Check if the email has really changed
        if ((this.updatedUser.email != undefined) &&
            (this.updatedUser.email != this.user.email)) {
            // Update the newly entered user email (communicate with the backend)
            this.authService.updateUserEmail(this.updatedUser).subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your email has been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
            });
        }
    };
    // Run when the user clicks the "Save changes" button while editing gender
    ProfileComponent.prototype.onClickUpdateGender = function () {
        var _this = this;
        if ((this.updatedUser.gender != undefined) &&
            (this.updatedUser.gender != this.user.gender)) {
            this.authService.updateUserGender(this.updatedUser).subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your gender has been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
            });
        }
    };
    // Run when the user clicks the "Save changes" button while editing
    // relationship status
    ProfileComponent.prototype.onClickUpdateRelationshipStatus = function () {
        var _this = this;
        if ((this.updatedUser.relationshipStatus != undefined) &&
            (this.updatedUser.relationshipStatus != this.user.relationshipStatus)) {
            this.authService.updateUserRelationshipStatus(this.updatedUser)
                .subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your relationship status has been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
            });
        }
    };
    // Run when the user clicks the "Save changes" button while editing school
    ProfileComponent.prototype.onClickUpdateSchool = function () {
        var _this = this;
        if ((this.updatedUser.school != undefined) &&
            (this.updatedUser.school != this.user.school)) {
            this.authService.updateUserSchool(this.updatedUser).subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your school has been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
            });
        }
    };
    // Run when the user clicks the "Save changes" button while editing school year
    ProfileComponent.prototype.onClickUpdateYearInSchool = function () {
        var _this = this;
        if ((this.updatedUser.yearInSchool != undefined) &&
            (this.updatedUser.yearInSchool != this.user.yearInSchool)) {
            this.authService.updateUserYearInSchool(this.updatedUser).
                subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your year in school has been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
            });
        }
    };
    // Run when the user clicks the "Save changes" button while editing major
    ProfileComponent.prototype.onClickUpdateMajor = function () {
        var _this = this;
        if ((this.updatedUser.major != undefined) &&
            (this.updatedUser.major != this.user.major)) {
            this.authService.updateUserMajor(this.updatedUser).subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your major has been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
            });
        }
    };
    // Run when the user clicks the "Update Preferences" button
    // NOTE: This function will always pass the entire updated preferences
    //       object, but this is alright because 1) MySQL will handle writes
    //       of the same items and 2) our previously-initialized blank object
    //       with dummy values has now been populated to be a mirror of the
    //       subscribed master object we are referencing
    ProfileComponent.prototype.onClickUpdatePreferences = function () {
        var _this = this;
        if (this.preferences != undefined) {
            this.authService.updatePreferences(this.preferences).subscribe(function (data) {
                if (data.success) {
                    _this.flashMessage.show('Your preferences have been updated.', { cssClass: 'alert-success', timeout: 3000 });
                    _this.loadProfileInfo();
                    _this.router.navigate(['/profile']);
                    _this.touchedSliders = false;
                }
                else {
                    _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                    _this.router.navigate(['/profile']);
                }
                window.scroll(0, 0); // Scroll to the top to see the flash message
            });
        }
    };
    // Run when the user clicks the "Delete" button in the delete account modal
    ProfileComponent.prototype.onClickDelete = function () {
        var _this = this;
        // NOTE: We only need to pass the user object; the backend will find any
        //       associated preferences and delete those as well for a clean delete
        this.authService.deleteUser(this.user).subscribe(function (data) {
            if (data.success) {
                _this.authService.logout(); // Still need to log out to clear the service
                _this.flashMessage.show('Your account has been deleted.', { cssClass: 'alert-success', timeout: 3000 });
                _this.router.navigate(['/']);
            }
            else {
                _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                _this.router.navigate(['/profile']);
            }
        });
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("./src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__("./src/app/components/profile/profile.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_6__models_user__["a" /* User */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h2 class=\"page-header\">Register</h2>\n  <form #registerForm=\"ngForm\" (submit)=\"onRegisterSubmit()\">\n    <div class=\"form-row\">\n      <div class=\"col-md mb-3\">\n        <label>First name</label>\n        <input type=\"text\" name=\"firstName_input\" [ngClass]=\"(firstName_input.invalid && (firstName_input.dirty || firstName_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"firstName\" #firstName_input=\"ngModel\">\n        <div *ngIf=\"firstName_input.invalid && (firstName_input.dirty || firstName_input.touched)\" class=\"invalid-feedback\">\n          <div *ngIf=\"firstName_input.errors.required\">\n            First name is required.\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md mb-3\">\n        <label>Last name</label>\n        <input type=\"text\" name=\"lastName_input\" [ngClass]=\"(lastName_input.invalid && (lastName_input.dirty || lastName_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"lastName\" #lastName_input=\"ngModel\">\n        <div *ngIf=\"lastName_input.invalid && (lastName_input.dirty || lastName_input.touched)\" class=\"invalid-feedback\">\n          <div *ngIf=\"lastName_input.errors.required\">\n            Last name is required.\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-row\">\n      <div class=\"col-md mb-3\">\n        <label>Email</label>\n        <input type=\"email\" name=\"email_input\" pattern=\"^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$\" [ngClass]=\"(email_input.invalid && (email_input.dirty || email_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"user.email\" #email_input=\"ngModel\" placeholder=\"name@example.com\">\n        <div *ngIf=\"email_input.invalid && (email_input.dirty || email_input.touched)\" class=\"invalid-feedback\">\n          <div *ngIf=\"email_input.errors.required\">\n            Email is required.\n          </div>\n          <div *ngIf=\"email_input.errors.pattern\">\n            Please enter a valid email.\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-row\">\n      <div class=\"col-md mb-3\">\n        <label>Password</label>\n        <input type=\"password\" name=\"password_input\" [ngClass]=\"(password_input.invalid && (password_input.dirty || password_input.touched)) ? 'form-control is-invalid' : 'form-control'\" required [(ngModel)]=\"user.password\" #password_input=\"ngModel\">\n        <div *ngIf=\"password_input.invalid && (password_input.dirty || password_input.touched)\" class=\"invalid-feedback\">\n          <div *ngIf=\"password_input.errors.required\">\n            Password is required.\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-row\">\n        <div class=\"col-md mb-3\">\n        <label>Gender</label>\n        <br>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.gender\" name=\"gender\" value=\"Male\" checked>\n          <label class=\"form-check-label\">Male</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.gender\" name=\"gender\" value=\"Female\">\n          <label class=\"form-check-label\">Female</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.gender\" name=\"gender\" value=\"Other\">\n          <label class=\"form-check-label\">Other</label>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-row\">\n        <div class=\"col-md mb-3\">\n        <label>Relationship Status</label>\n        <br>\n        <select class=\"form-control\" [(ngModel)]=\"user.relationshipStatus\" name=\"relationshipStatus\">\n          <option>Single</option>\n          <option>In a relationship</option>\n          <option>Engaged</option>\n          <option>Married</option>\n          <option>Widowed</option>\n          <option>Separated</option>\n          <option>Divorced</option>\n          <option>In a civil union</option>\n          <option>In a domestic partnership</option>\n          <option>Prefer not to say</option>\n        </select>\n      </div>\n    </div>\n    <div class=\"form-row\">\n        <div class=\"col-md mb-3\">\n        <label>School</label>\n        <br>\n        <select class=\"form-control\" [(ngModel)]=\"user.school\" name=\"school\">\n          <option *ngFor=\"let school of schools\" [value]=\"school.schoolName\">\n            {{school.schoolName}}\n          </option>\n        </select>\n      </div>\n    </div>\n    <div class=\"form-row\">\n        <div class=\"col-md mb-3\">\n        <label>Year in School</label>\n        <br>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.yearInSchool\" name=\"yearInSchool\" value=\"First\" checked>\n          <label class=\"form-check-label\">First</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.yearInSchool\" name=\"yearInSchool\" value=\"Second\">\n          <label class=\"form-check-label\">Second</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.yearInSchool\" name=\"yearInSchool\" value=\"Third\">\n          <label class=\"form-check-label\">Third</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.yearInSchool\" name=\"yearInSchool\" value=\"Fourth\">\n          <label class=\"form-check-label\">Fourth</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" [(ngModel)]=\"user.yearInSchool\" name=\"yearInSchool\" value=\"Fifth\">\n          <label class=\"form-check-label\">Fifth</label>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-row\">\n      <div class=\"col-md mb-3\">\n        <label>Major</label>\n        <input type=\"text\" name=\"major_input\" class=\"form-control\" [(ngModel)]=\"user.major\" #major_input=\"ngModel\">\n      </div>\n    </div>\n    <br>\n    <div>\n      <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"registerForm.invalid\">Submit</button>\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"registerForm.resetForm(user = {gender: 'Male', relationshipStatus: 'Single', school: 'No school selected...'})\">Reset</button>\n    </div>\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("./src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_user__ = __webpack_require__("./src/app/models/user.ts");
/**
 * @file Angular register component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(router, authService, dataService, flashMessage) {
        this.router = router;
        this.authService = authService;
        this.dataService = dataService;
        this.flashMessage = flashMessage;
        // Instantiate a new User object for binding with form elements
        this.user = new __WEBPACK_IMPORTED_MODULE_5__models_user__["a" /* User */]();
    }
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Initialize some values for sake of having the form appear correctly
        this.user.gender = "Male";
        this.user.relationshipStatus = "Single";
        this.user.school = "No school selected..."; // Placeholder value
        // Grab all of the schools from the DB
        this.dataService.getAllSchools().subscribe(function (results) {
            _this.schools = results;
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    // New user registration procedure, called by clicking the "Register" button
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        // Concatenate the final first and last names
        this.user.fullName = this.firstName + " " + this.lastName;
        // Register the user (communicate with the backend)
        this.authService.registerUser(this.user).subscribe(function (data) {
            if (data.success) {
                _this.flashMessage.show('You are now registered and can log in.', { cssClass: 'alert-success', timeout: 3000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this.flashMessage.show('Something went wrong. Please try again.', { cssClass: 'alert-danger', timeout: 3000 });
                _this.router.navigate(['/register']);
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("./src/app/components/register/register.component.html"),
            styles: [__webpack_require__("./src/app/components/register/register.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_5__models_user__["a" /* User */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/**
 * @file Angular authorization guard.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    // Verifies that the user is logged in to complete the request
    AuthGuard.prototype.canActivate = function () {
        // If the user is logged in, return true
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/models/preference.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Preference; });
/**
 * @file Frontend preference model class that matches the backend schema.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var Preference = /** @class */ (function () {
    function Preference() {
    }
    // Fill in default values
    // NOTE: This is not strictly necessary, as we could rely on the DB to take
    //       care of defaults, but this lessens the chance of an error.
    Preference.prototype.initDefaults = function () {
        this.party = 0;
        this.sleep = 0;
        this.noiseLevel = 0;
        this.petPreference = true;
        this.firstChoiceNeighborhood = 'No neighborhood selected...';
        this.utilitiesIncluded = true;
        this.maxNumofRoommates = 1;
        this.minBudget = 0;
        this.maxBudget = 9999;
    };
    return Preference;
}());



/***/ }),

/***/ "./src/app/models/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
/**
 * @file Frontend user model class that matches the backend schema.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var User = /** @class */ (function () {
    function User() {
    }
    // Populate the object with blank values to supress rendering errors
    User.prototype.fillInPlaceholders = function () {
        this.userid = null;
        this.email = "";
        this.fullName = "";
        this.major = "";
        this.gender = "";
        this.yearInSchool = "";
        this.relationshipStatus = "";
        this.preferenceid = null;
    };
    return User;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__("./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_environment__ = __webpack_require__("./src/config/environment.ts");
/**
 * @file Angular authorization service.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
    }
    // Attempt to register the user with the backend
    AuthService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        // Make the POST request
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/register', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to authenticate the user with the backend
    AuthService.prototype.authenticateUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/authenticate', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get the user profile information from the backend
    AuthService.prototype.getProfile = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken(); // Grab the authorization token
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/profile', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to create and link the preferences profile with the backend (requires authentication)
    AuthService.prototype.createPreferences = function (user, preferences) {
        // Here, we need to send both the user and the new preferences
        var payload = Object.assign({}, user, preferences);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/createprefs', payload, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user email in the database
    AuthService.prototype.updateUserEmail = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updateemail', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user gender in the database
    AuthService.prototype.updateUserGender = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updategender', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user relationship status in the database
    AuthService.prototype.updateUserRelationshipStatus = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updaterelationship', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user relationship status in the database
    AuthService.prototype.updateUserSchool = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updateschool', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user year in school in the database
    AuthService.prototype.updateUserYearInSchool = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updateyear', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user major in the database
    AuthService.prototype.updateUserMajor = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updatemajor', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to update the user preferences in the database
    AuthService.prototype.updatePreferences = function (preferences) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/updateprefs', preferences, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to delete the user from the database
    AuthService.prototype.deleteUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/users/remove', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Store the token in local storage
    AuthService.prototype.storeUserData = function (token) {
        localStorage.setItem('id_token', token);
        this.authToken = token;
    };
    // Load the token from local storage
    AuthService.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authToken = token;
    };
    // Return true if the user is logged in, false otherwise
    AuthService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["tokenNotExpired"])('id_token');
    };
    // Clear the token, user object, and local storage
    AuthService.prototype.logout = function () {
        this.authToken = null;
        localStorage.clear();
    };
    // NOTE: The rest of the functions are more suited to exist in the data
    //       service, but protected requests need to remain here to share the
    //       auth token
    // Attempt to get the list of roommates from the backend
    AuthService.prototype.getAllRoommates = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getroommates', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates at the same school
    AuthService.prototype.getSchoolmates = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getschoolmates', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates in the same major
    AuthService.prototype.getSameMajors = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getsamemajors', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates at the same school
    AuthService.prototype.getSameYears = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getsameyears', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates preferring the same location
    AuthService.prototype.getSameLocation = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getsameloc', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates with similar budgets
    AuthService.prototype.getSimilarBudgets = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getsimilarbudgets', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates who prefer pets (pass in the last query result)
    AuthService.prototype.getPreferPets = function (lastPressedButton, utilitiesCheck) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        var search = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["URLSearchParams"]();
        search.set('appliedFilter', lastPressedButton);
        search.set('utilitiesCheck', utilitiesCheck);
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getpreferpets', { headers: headers, search: search })
            .map(function (res) { return res.json(); });
    };
    // Attempt to get roommates who prefer utilities
    AuthService.prototype.getPreferUtilities = function (lastPressedButton, petCheck) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        var search = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["URLSearchParams"](); // For appending queries
        search.set('appliedFilter', lastPressedButton);
        search.set('petCheck', petCheck);
        this.loadToken();
        headers.append('Authorization', this.authToken); // Protected request
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__config_environment__["a" /* baseUrl */] + '/data/getpreferutils', { headers: headers, search: search })
            .map(function (res) { return res.json(); });
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_environment__ = __webpack_require__("./src/config/environment.ts");
/**
 * @file Angular data service.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
    }
    // Get all of the neighborhoods from the backend
    DataService.prototype.getAllNeighborhoods = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_environment__["a" /* baseUrl */] + '/data/getneighborhoods', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Get all of the schools from the backend
    DataService.prototype.getAllSchools = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_environment__["a" /* baseUrl */] + '/data/getschools', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/config/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return baseUrl; });
/**
 * @file Environment configuration file for frontend.
 * @author Ben Prisby <bdprisby@gmail.com>
 */
// Set this to true if you are planning on running the local dev server,
// or false for deployment.
var DEVELOPMENT_MODE = false;
var baseUrl = DEVELOPMENT_MODE ? "http://localhost:3000" : "";


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map