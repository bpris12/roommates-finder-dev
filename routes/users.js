/**
 * @file Express routes for user-related tasks.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const db = require('../config/db');

// Register a new user
router.post('/register', (req, res, next) => {
  // Pull out all of the necessary parameters from the request
  let email = req.body.email;
  let userPassword = req.body.password;
  let fullName = req.body.fullName;
  let gender = req.body.gender;
  let relationshipStatus = req.body.relationshipStatus;
  let school = req.body.school;
  let yearInSchool = req.body.yearInSchool;
  let major = req.body.major;

  // NOTE: Password hashing was causing problems when the hash was being
  //       stored back into the database. MySQL kept storing the string as
  //       undefined, even with legal characters. Not sure why this was the
  //       case, but until it is figured out, just store the plaintext password.

  // Insert into table
  let sql = "INSERT INTO users (email, userPassword, fullName, gender, \
    relationshipStatus, school, yearInSchool, major) VALUES \
    ('"+email+"', '"+userPassword+"', '"+fullName+"', '"+gender+"', \
    '"+relationshipStatus+"', '"+school+"', '"+yearInSchool+"', '"+major+"')";
  db.query(sql, (err, result) => {
    if (err){
      res.json({success: false, msg: 'Failed to register user.'});
    }
    else {
      res.json({success: true, msg: 'User registered.'});
    }
  });
});

// Authenticate a user (such as when logging in)
router.post('/authenticate', (req, res, next) => {
  // NOTE: Email addresses may not necessarily be unique, as they are NOT the
  //       primary key according to the schema. So, either enforce uniqueness
  //       in the schema or have users supply user IDs (pk) to prevent
  //       unexpected behavior (unlikely, but possible!)

  // Pull out the email and password from the request
  const email = req.body.email;
  const password = req.body.password;

  // Query the database for the user based on the email
  let sql = "SELECT * FROM users WHERE email = '"+email+"'";
  db.query(sql, (err, result) => {
    if(err){
        throw err;
    }

    // If we got back an empty set, the user does not exist
    if(result == undefined || result == 0) {
      return res.json({success: false, msg: 'User not found.'});
    }

    // If we got a match, sign the token and send back success
    if(result[0].userPassword == password){
      const payload = {
        userid: result[0].userid,
        email: result[0].email
      };

      const token = jwt.sign(payload, db.secret, {
        expiresIn: 86400 // 1 day (in seconds)
      });

      // Send back a confirmation message with the token and basic user info
      res.json({
        success: true,
        token: 'JWT ' + token,
        message: 'Token created!'
      });
    }
    // The password did not match, so send back failure
    else{
      return res.json({success: false, msg: 'Wrong password.'});
    }
  });
});

// User profile (requires authentication)
router.get('/profile', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // Just send back the "user" part, which is the user and linked preferences
  res.json({user: req.user});
});

// Create and link preferences profile (requires authentication)
router.post('/createprefs', (req, res, next) => {
  // Pull out the user id from the request
  const userid = req.body.userid;

  // Check for a valid user first
  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    // We have a valid user, so grab the preferences and enter them into the DB
    const party = req.body.party;
    const sleep = req.body.sleep;
    const noiseLevel = req.body.noiseLevel;
    const firstChoiceNeighborhood = req.body.firstChoiceNeighborhood;
    const maxNumofRoommates = req.body.maxNumofRoommates;
    const minBudget = req.body.minBudget;
    const maxBudget = req.body.maxBudget;

    // MySQL requires integers for boolean values, so convert
    let petPreference;
    let utilitiesIncluded;
    if(req.body.petPreference == true){
      petPreference = 1;
    }
    else {
      petPreference = 0;
    }
    if(req.body.utilitiesIncluded == true){
      utilitiesIncluded = 1;
    }
    else {
      utilitiesIncluded = 0;
    }

    let insertQuery = "INSERT INTO preferences (party, sleep, noiseLevel, \
      petPreference, firstChoiceNeighborhood, utilitiesIncluded, \
      maxNumofRoommates, minBudget, maxBudget) VALUES ('"+party+"', \
      '"+sleep+"', '"+noiseLevel+"', '"+petPreference+"', \
      '"+firstChoiceNeighborhood+"', '"+utilitiesIncluded+"', \
      '"+maxNumofRoommates+"', '"+minBudget+"', '"+maxBudget+"')";
    db.query(insertQuery, (err, result) => {
      // Send a JSON response indicating success/failure
      if(err) {
        console.log(err);
        res.json({success: false, msg: 'Failed to insert preferences row'});
      }
      else {
        // Link the preferences to the user
        let linkQuery = "UPDATE users SET preferenceid = (SELECT \
          MAX(preferenceId) FROM preferences) WHERE userid='"+userid+"'";
        db.query(linkQuery, (err, result) => {
          if (err){
            res.json({success: false, msg: 'Failed to link preferences to\
             user'});
          }
          else {
            res.json({success: true, msg: 'Preferences created and linked'});
          }
        });
      }
    });
  });
});

// Update a user's email (requires authentication)
router.post('/updateemail', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // Pull out the user id and new email from the request
  const userid = req.body.userid;
  const newEmail = req.body.email;

  // Check for a valid user
  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    // Update the email for the user's row
    let updateQuery = "UPDATE users SET email = '"+newEmail+"'\
     WHERE userid='"+userid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update email'});
      }
      else {
        res.json({success: true, msg: 'Email updated'});
      }
    });
  });
});

// Update a user's gender (requires authentication)
router.post('/updategender', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  const userid = req.body.userid;
  const newGender = req.body.gender;

  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    let updateQuery = "UPDATE users SET gender = '"+newGender+"'\
     WHERE userid='"+userid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update gender'});
      }
      else {
        res.json({success: true, msg: 'Email updated'});
      }
    });
  });
});

// Update a user's relationship status (requires authentication)
router.post('/updaterelationship', passport.authenticate('jwt',
              {session: false}), (req, res, next) => {
  const userid = req.body.userid;
  const newRelationshipStatus = req.body.relationshipStatus;

  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    let updateQuery = "UPDATE users SET relationshipStatus =\
     '"+newRelationshipStatus+"' WHERE userid='"+userid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update relationship status'});
      }
      else {
        res.json({success: true, msg: 'Email updated'});
      }
    });
  });
});

// Update a user's school (requires authentication)
router.post('/updateschool', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  const userid = req.body.userid;
  const newSchool = req.body.school;

  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    let updateQuery = "UPDATE users SET school = '"+newSchool+"'\
     WHERE userid='"+userid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update school'});
      }
      else {
        res.json({success: true, msg: 'Email updated'});
      }
    });
  });
});

// Update a user's year in school (requires authentication)
router.post('/updateyear', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  const userid = req.body.userid;
  const newYearInSchool = req.body.yearInSchool;

  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    let updateQuery = "UPDATE users SET yearInSchool = '"+newYearInSchool+"'\
     WHERE userid='"+userid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update year in school'});
      }
      else {
        res.json({success: true, msg: 'Email updated'});
      }
    });
  });
});

// Update a user's major (requires authentication)
router.post('/updatemajor', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  const userid = req.body.userid;
  const newMajor = req.body.major;

  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    let updateQuery = "UPDATE users SET major = '"+newMajor+"' WHERE\
     userid='"+userid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update major'});
      }
      else {
        res.json({success: true, msg: 'Email updated'});
      }
    });
  });
});

// Update a user's preferences (requires authentication)
router.post('/updateprefs', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // Pull out the preference id from the request
  const preferenceid = req.body.preferenceid;

  // Check for a valid preferences row
  let sql = "SELECT * FROM preferences WHERE preferenceid='"+preferenceid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'Preferences not found'});
    }

    // Grab the rest of the parameters from the request and update them
    // NOTE: Here, we may overwrite with the same value, but that's fine
    const party = req.body.party;
    const sleep = req.body.sleep;
    const noiseLevel = req.body.noiseLevel;
    const firstChoiceNeighborhood = req.body.firstChoiceNeighborhood;
    const maxNumofRoommates = req.body.maxNumofRoommates;
    const minBudget = req.body.minBudget;
    const maxBudget = req.body.maxBudget;

    // MySQL requires integers for boolean values, so convert
    let petPreference;
    let utilitiesIncluded;
    if(req.body.petPreference == true){
      petPreference = 1;
    }
    else {
      petPreference = 0;
    }
    if(req.body.utilitiesIncluded == true){
      utilitiesIncluded = 1;
    }
    else {
      utilitiesIncluded = 0;
    }

    let updateQuery = "UPDATE preferences SET party = '"+party+"',\
     sleep = '"+sleep+"', noiseLevel = '"+noiseLevel+"',\
      petPreference = '"+petPreference+"',\
       firstChoiceNeighborhood = '"+firstChoiceNeighborhood+"',\
        utilitiesIncluded = '"+utilitiesIncluded+"',\
         maxNumofRoommates = '"+maxNumofRoommates+"',\
          minBudget = '"+minBudget+"', maxBudget = '"+maxBudget+"'\
           WHERE preferenceid='"+preferenceid+"'"
    db.query(updateQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to update preferences'});
      }
      else {
        res.json({success: true, msg: 'Preferences updated'});
      }
    });
  });
});

// Delete a user and corresponding preferences (requires authentication)
router.post('/remove', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // Pull out the user and preference id from the request
  const userid = req.body.userid;
  const preferenceid = req.body.preferenceid;

  // Check for a valid user
  let sql = "SELECT * FROM users WHERE userid='"+userid+"'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }

    if(result == undefined || result.length == 0) {
      return res.json({success: false, msg: 'User not found'});
    }

    // First, delete the user's row, then the preferences
    let delQuery = "DELETE FROM users WHERE userid='"+userid+"'";
    db.query(delQuery, (err, result) => {
      if(err) {
        res.json({success: false, msg: 'Failed to delete user'}); // Stop
      }
      // NOTE: The following code should be commented out if using the trigger
      //       to delete user preferences automatically. However, the MySQL
      //       host I used does not allow triggers for a free account, so we
      //       have to run the query ourselves here.

      // Ensure there are preferences associated with the user
      else if (preferenceid != undefined) {
        let delPrefsQuery = "DELETE FROM preferences WHERE\
         preferenceid='"+preferenceid+"'";
        db.query(delPrefsQuery, (err, result) => {
          if(err) {
            res.json({success: false, msg: 'Deleted user, but failed\
             to delete preferences'});
          }
          else {
            res.json({success: true, msg: 'User and preferences deleted'});
          }
        });
      }
      else {
        res.json({success: true, msg: 'User deleted'});
      }
    });
  });
});

module.exports = router;
