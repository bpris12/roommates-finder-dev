/**
 * @file Express routes for data-oriented tasks.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

const express = require('express');
const router = express.Router();
const db = require('../config/db');
const passport = require('passport');
const jwt = require('jsonwebtoken');

// Get all neighborhoods
router.get('/getneighborhoods', (req, res) => {
  // Create a union statement to force the default option to appear first
  let sql = "SELECT * FROM neighborhoods WHERE\
   neighborhoodName = 'No neighborhood selected...' UNION SELECT *\
    FROM neighborhoods WHERE neighborhoodName != 'No neighborhood selected...'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all schools
router.get('/getschools', (req, res) => {
  let sql = "SELECT * FROM schools WHERE schoolName = 'No school selected...'\
   UNION SELECT * FROM schools WHERE schoolName != 'No school selected...'";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates (requires authentication)
router.get('/getroommates', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // Send back everything except the password for security
  let sql = 'SELECT userid, email, fullName, gender, relationshipStatus,\
   school, yearInSchool, major, users.preferenceid, party, sleep, noiseLevel,\
    petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
     maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
      preferences ON users.preferenceid = preferences.preferenceid\
       ORDER BY fullName';
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates at the same school as the user (requires authentication)
router.get('/getschoolmates', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  let sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
   school, yearInSchool, major, users.preferenceid, party, sleep, noiseLevel,\
    petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
     maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
      preferences ON users.preferenceid = preferences.preferenceid WHERE\
       school='"+req.user.user.school+"' ORDER BY fullName";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates in the same major as the user (requires authentication)
router.get('/getsamemajors', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  let sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
   school, yearInSchool, major, users.preferenceid, party, sleep, noiseLevel,\
    petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
     maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
      preferences ON users.preferenceid = preferences.preferenceid WHERE\
       major='"+req.user.user.major+"' ORDER BY fullName";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates who are the same year in school as the user
// (requires authentication)
router.get('/getsameyears', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  let sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
   school, yearInSchool, major, users.preferenceid, party, sleep, noiseLevel,\
    petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
     maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
      preferences ON users.preferenceid = preferences.preferenceid WHERE\
       yearInSchool='"+req.user.user.yearInSchool+"' ORDER BY fullName";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates preferring the same location as the user
// (requires authentication)
router.get('/getsameloc', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  let sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
   school, yearInSchool, major, users.preferenceid, party, sleep, noiseLevel,\
    petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
     maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
      preferences ON users.preferenceid = preferences.preferenceid WHERE\
       firstChoiceNeighborhood=\
       '"+req.user.preferences.firstChoiceNeighborhood+"' ORDER BY fullName";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates with similar budgets to the user (requires authentication)
router.get('/getsimilarbudgets', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // To be considered similar, min and max budget constraints should be
  // within $500 of each other (take the absolute value of the difference)
  let sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
   school, yearInSchool, major, users.preferenceid, party, sleep, noiseLevel,\
    petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
     maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
      preferences ON users.preferenceid = preferences.preferenceid WHERE\
       ABS(minBudget - '"+req.user.preferences.minBudget+"') <= 500 AND\
        ABS(maxBudget - '"+req.user.preferences.maxBudget+"') <= 500\
         ORDER BY fullName";
  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates that prefer pets, minding what the user is currently
// looking at (requires authentication)
router.get('/getpreferpets', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  // See what the last query was to determine the right one to run
  // NOTE: This is not strictly RESTful in design, as this specifically
  //       corresponds to the Popular Searches buttons on the Dashboard in
  //       the frontend, but it's better to have the backend run specific
  //       queries than making sorting/filtering the responsibility of the
  //       frontend (in my opinion)
  let sql;

  // The appliedFilter field of the query tells us which button is active
  // TODO: Modularizing these query strings would decrease repetition
  switch(req.query.appliedFilter){
    case 'all':
      sql = 'SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE petPreference = 1';
      break;
    case 'school':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE school='"+req.user.user.school+"' AND petPreference = 1";
      break;
    case 'major':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE major='"+req.user.user.major+"' AND petPreference = 1";
      break;
    case 'year':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE yearInSchool='"+req.user.user.yearInSchool+"' AND\
            petPreference = 1";
      break;
    case 'location':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE firstChoiceNeighborhood=\
           '"+req.user.preferences.firstChoiceNeighborhood+"' AND\
            petPreference = 1";
      break;
    case 'budget':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE ABS(minBudget - '"+req.user.preferences.minBudget+"') <= 500\
            AND ABS(maxBudget - '"+req.user.preferences.maxBudget+"') <= 500\
             AND petPreference = 1";
      break;
  }

  // Check if the utilities box was also selected and append the query if so
  if(req.query.utilitiesCheck == 'true'){
    sql += " AND utilitiesIncluded = 1";
  }

  sql += " ORDER BY fullName"; // Add correct display ordering

  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

// Get all roommates that prefer utilities, similar to above
// (requires authentication)
router.get('/getpreferutils', passport.authenticate('jwt', {session: false}),
                                              (req, res, next) => {
  let sql;

  switch(req.query.appliedFilter){
    case 'all':
      sql = 'SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE utilitiesIncluded = 1';
      break;
    case 'school':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE school='"+req.user.user.school+"' AND utilitiesIncluded = 1";
      break;
    case 'major':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE major='"+req.user.user.major+"' AND utilitiesIncluded = 1";
      break;
    case 'year':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE yearInSchool='"+req.user.user.yearInSchool+"' AND\
            utilitiesIncluded = 1";
      break;
    case 'location':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE firstChoiceNeighborhood=\
           '"+req.user.preferences.firstChoiceNeighborhood+"' AND\
            utilitiesIncluded = 1";
      break;
    case 'budget':
      sql = "SELECT userid, email, fullName, gender, relationshipStatus,\
       school, yearInSchool, major, users.preferenceid, party, sleep,\
        noiseLevel, petPreference, firstChoiceNeighborhood, utilitiesIncluded,\
         maxNumofRoommates, minBudget, maxBudget FROM users LEFT OUTER JOIN\
          preferences ON users.preferenceid = preferences.preferenceid\
           WHERE ABS(minBudget - '"+req.user.preferences.minBudget+"') <= 500\
            AND ABS(maxBudget - '"+req.user.preferences.maxBudget+"') <= 500\
             AND utilitiesIncluded = 1";
      break;
  }

  if(req.query.petCheck == 'true'){
    sql += " AND petPreference = 1";
  }

  sql += " ORDER BY fullName";

  db.query(sql, (err, result) => {
    if (err){
      throw err;
    }
    res.send(result);
  });
});

module.exports = router;
