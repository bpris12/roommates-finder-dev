/* File: MySQL Database initialization file (with trigger)
 * Author: Ben Prisby <bdprisby@gmail.com>
 */

/* NOTE: This drop/creation procedure may need to be slightly modified if
 * the database host does not allow for creation/deletion at the database level.
 * Be sure to update ./config/db.js accordingly!
 */
DROP DATABASE IF EXISTS roommates;
CREATE DATABASE roommates;
USE roommates;

CREATE TABLE neighborhoods(
	neighborhoodName VARCHAR(255) PRIMARY KEY,
	description VARCHAR(255),
	zipCode CHAR(5) NOT NULL,
	publicTransportation BOOLEAN NOT NULL,
	typeofAccommodations VARCHAR(255)
);

CREATE TABLE schools(
	schoolName VARCHAR(255) PRIMARY KEY,
	neighborhood VARCHAR(255) NOT NULL,
	hasHousing BOOLEAN NOT NULL,
	CONSTRAINT neighborhood_fk
		FOREIGN KEY (neighborhood) REFERENCES neighborhoods(neighborhoodName)
);

CREATE TABLE preferences(
	preferenceid INT AUTO_INCREMENT PRIMARY KEY,
	party INT DEFAULT 0,
	sleep INT DEFAULT 0,
	noiseLevel INT DEFAULT 0,
	petPreference BOOLEAN NOT NULL,
	firstChoiceNeighborhood VARCHAR(255),
	utilitiesIncluded BOOLEAN,
	maxNumofRoommates INT NOT NULL,
	minBudget INT DEFAULT 0,
	maxBudget INT DEFAULT 9999,
	CONSTRAINT neighborhoodChoice_fk
		FOREIGN KEY (firstChoiceNeighborhood) REFERENCES neighborhoods(neighborhoodName) ON DELETE SET NULL
);


CREATE TABLE users(
	userid INT AUTO_INCREMENT PRIMARY KEY,
	email VARCHAR(255) NOT NULL,
	userPassword CHAR(60) NOT NULL,
	fullName VARCHAR(255) NOT NULL,
	gender VARCHAR(255) NOT NULL,
	relationshipStatus VARCHAR(255),
	school VARCHAR(255),
	yearInSchool VARCHAR(255),
	major VARCHAR(255),
	preferenceid INT,
	CONSTRAINT school_fk
		FOREIGN KEY (school) REFERENCES schools(schoolName),
	CONSTRAINT preferencesId_fk
		FOREIGN KEY (preferenceId) REFERENCES preferences(preferenceId) ON DELETE SET NULL,
	CONSTRAINT unique_Email
		UNIQUE (email)
);

DELIMITER $$
CREATE TRIGGER delete_prefs_after_user_delete
	AFTER DELETE ON users
    FOR EACH ROW
BEGIN
	IF OLD.preferenceid IS NOT NULL THEN
		DELETE FROM preferences WHERE preferences.preferenceid = OLD.preferenceid;
	END IF;
END $$

DELIMITER ;

INSERT INTO neighborhoods VALUES
('No neighborhood selected...', 'Placeholder', '00000', 0, 'N/A'),
('Allston', 'One of Boston’s most diverse and active neighborhoods.', '02134', 1, 'All'),
('Back Bay', 'One of the most picture-perfect parts of the City of Boston.', '02116', 1, 'All'),
('Bay Village', 'Bay Village is a brick rowhouse oasis.', '02116', 1, 'All'),
('Beacon Hill', 'The historic Beacon Hill is a place frozen in time.', '02108', 1, 'All'),
('Brighton', 'This dynamic and peaceful neighborhood is a great place for young families and professionals.', '02135', 1, 'All'),
('Charlestown', 'Explore the Irish roots and Naval History, and walk along the Freedom Trail.', '02129', 1, 'All'),
('Chinatown-Leather District', 'Neighborhoods with truly unique culture.', '02111', 1, 'All'),
('Dorchester', 'The biggest and most diverse neighborhood in the City.', '02121', 1, 'All'),
('Downtown', 'The bustling downtown district.', '02201', 1, 'All'),
('East Boston', 'The vibrant neighborhood features restaurants, shops, and one of the most beautiful waterfronts in the City.', '02128', 1, 'All'),
('Fenway-Kenmore', 'A lively neighborhood with many schools and activities.', '02115', 1, 'All'),
('Hyde Park', 'A neighborhood that offers the best of both worlds.', '02136', 1, 'All'),
('Jamaica Plain', 'One of the most diverse and happening neighborhoods.', '02130', 1, 'All'),
('Mattapan', 'Home to diverse cultures and a large amount of immigrant-owned businesses.', '02126', 1, 'All'),
('Mission Hill', 'The most convenient and diverse neighborhood.', '02120', 1, 'All'),
('North End', 'A tight-knit community in the North End based on Italian culture.', '02113', 1, 'All'),
('Roslindale', 'Neighborhood with natural beauty and charm.', '02131', 1, 'All'),
('Roxbury', 'Neighborhood with historic culture and presence', '02119', 1, 'All'),
('South Boston', 'An urban neighborhood with a strong sense of history and tradition.', '02127', 1, 'All'),
('South End', 'This centrally-located neighborhood has something for everyone.', '02118', 1, 'All'),
('West End', 'A small but historic neighborhood, the West End is a mix of old and new Boston.', '02114', 1, 'All'),
('West Roxbury', 'A small but friendly suburban community.', '02132', 1, 'All');

INSERT INTO schools VALUES
('No school selected...', 'No neighborhood selected...', 0),
('Bay State College', 'Back Bay', 1),
('Boston Architectural College', 'Back Bay', 0),
('Suffolk University', 'Beacon Hill', 1),
('Bunker Hill Community College', 'Beacon Hill', 1),
('Brighton College', 'Brighton', 1),
('Boston College', 'Brighton', 1),
('Cambridge College', 'Charlestown', 1),
('UMASS Medical School', 'Charlestown', 1),
('Babson College', 'Downtown', 0),
('Trustees of Tufts College', 'Downtown', 1),
('MCPHS', 'Fenway-Kenmore', 1),
('Harvard Medical School', 'Fenway-Kenmore', 1),
('Emerson College', 'Downtown', 1),
('Massachusetts College of Art and Design', 'Fenway-Kenmore', 1),
('Northeastern University', 'Fenway-Kenmore', 1),
('New England Conservatory', 'Back Bay', 1),
('Berklee College of Music', 'Back Bay', 1),
('Wentworth Institute of Technology', 'Fenway-Kenmore', 1),
('Simmons College', 'Fenway-Kenmore', 1),
('Emmanuel College', 'Downtown', 1),
('Boston University', 'Allston', 1),
('Roxbury Community College', 'Roxbury', 0);

INSERT INTO preferences VALUES
(1, 10, 5, 2, 1, 'Back Bay', 1, 5, 500, 1500),
(2, 5, 8, 7, 1, 'Beacon Hill', 0, 2, 1025, 2300),
(3, 9, 6, 1, 0, 'Back Bay', 1, 4, 975, 2150),
(4, 0, 9, 6, 1, 'Bay Village', 0, 4, 900, 1600),
(5, 6, 10, 2, 0, 'South Boston', 0, 3, 775, 1400),
(6, 8, 4, 6, 1, 'Allston', 1, 8, 425, 1025),
(7, 10, 9, 7, 1, 'South End', 0, 7, 500, 1750),
(8, 7, 4, 3, 0, 'Fenway-Kenmore', 1, 6, 300, 1000),
(9, 0, 8, 7, 1, 'Back Bay', 0, 2, 3500, 8400),
(10, 0, 4, 2, 0, 'Bay Village', 0, 2, 800, 1675),
(11, 2, 3, 7, 0, 'Fenway-Kenmore', 1, 3, 1500, 2050),
(12, 10, 1, 8, 1, 'Roxbury', 0, 4, 300, 5000),
(13, 5, 0, 3, 1, 'Roslindale', 1, 7, 2500, 7600),
(14, 0, 10, 4, 0, 'Allston', 0, 5, 100, 900),
(15, 1, 6, 10, 1, 'Back Bay', 1, 3, 500, 1000),
(16, 2, 5, 9, 1, 'Roxbury', 0, 5, 3000, 6000),
(17, 5, 4, 0, 1, 'Fenway-Kenmore', 0, 3, 3025, 6000),
(18, 10, 1, 4, 0, 'South Boston', 0, 3, 400, 2150),
(19, 0, 4, 10, 1, 'West End', 0, 4, 1750, 3000),
(20, 3, 2, 5, 0, 'Roxbury', 1, 2, 600, 1575),
(21, 1, 3, 8, 1, 'Roslindale', 1, 4, 1000, 2150),
(22, 0, 2, 3, 0, 'Fenway-Kenmore', 0, 6, 4000, 8000),
(23, 4, 8, 9, 1, 'Beacon Hill', 1, 7, 750, 2000),
(24, 7, 6, 3, 0, 'South End', 1, 5, 2000, 3500),
(25, 1, 10, 4, 0, 'Bay Village', 1, 6, 1025, 1200);

INSERT INTO users VALUES
(1, 'patrick@perkins.com', '1234abcd', 'Patrick Perkins', 'Male', 'In a relationship', 'MCPHS', 'First', 'Dentistry', 1),
(2, 'david@johnson.com', '1234abcd', 'David Johnson', 'Male', 'Married', 'New England Conservatory', 'Fifth', 'Composition', 2),
(3, 'christina@matthews.com', '1234abcd', 'Christina Matthews', 'Female', 'Single', 'Emerson College', 'Second', 'English', 3),
(4, 'earl@ginn.com', '1234abcd', 'Earl Ginn', 'Male', 'Married', 'Boston University', 'Fourth', 'English', 4),
(5, 'vito@levin.com', '1234abcd', 'Vito Levin', 'Male', 'In a relationship', 'UMASS Medical School', 'Third', 'Nursing', 5),
(6, 'carrie@lexington.com', '1234abcd', 'Carrie Lexington', 'Female', 'Married', 'Northeastern University', 'First', 'Computer Science', 6),
(7, 'josh@rodriguez.com', '1234abcd', 'Josh Rodriguez', 'Male', 'Single', 'Bunker Hill Community College', 'Second', 'Chemistry', 7),
(8, 'carol@white.com', '1234abcd', 'Carol White', 'Female', 'Single', 'Northeastern University', 'Fifth', 'Business', 8),
(9, 'phoebe@smith.com', '1234abcd', 'Phoebe Smith', 'Female', 'Prefer not to say', 'MCPHS', 'First', 'Biology', 9),
(10, 'courtney@miser.com', '1234abcd', 'Courtney Miser', 'Female', 'Single', 'Wentworth Institute of Technology', 'Third', 'IT', 10),
(11, 'jeffrey@stuart.com', '1234abcd', 'Jeffrey Stuart', 'Male', 'In a domestic partnership', 'Massachusetts College of Art and Design', 'Second', 'Fine Arts', 11),
(12, 'kevin@miller.com', '1234abcd', 'Kevin Miller', 'Male', 'In a civil union', 'Roxbury Community College', 'Second', 'Business', 12),
(13, 'roberta@gary.com', '1234abcd', 'Roberta Gary', 'Female', 'Single', 'Emmanuel College', 'Third', 'Fashion Design', 13),
(14, 'valerie@gonzalez.com', '1234abcd', 'Valerie Gonzalez', 'Female', 'Engaged', 'Harvard Medical School', 'Fourth', 'Nursing', 14),
(15, 'stewart@davies.com', '1234abcd', 'Stewart Davies', 'Male', 'Married', 'Suffolk University', 'First', 'English', 15),
(16, 'garrett@whitley.com', '1234abcd', 'Garrett Whitley', 'Male', 'Prefer not to say', 'Massachusetts College of Art and Design', 'Fourth', 'Fine Arts', 16),
(17, 'mary@tucker.com', '1234abcd', 'Mary Tucker', 'Female', 'In a relationship', 'MCPHS', 'Fifth', 'Chemistry', 17),
(18, 'kendall@williams.com', '1234abcd', 'Kendall Williams', 'Female', 'Single', 'Northeastern University', 'First', 'Computer Science', 18),
(19, 'daniel@harris.com', '1234abcd', 'Daniel Harris', 'Male', 'Married', 'Brighton College', 'Fourth', 'Communications', 19),
(20, 'roger@taylor.com', '1234abcd', 'Roger Taylor', 'Male', 'In a civil union', 'Boston University', 'Second', 'Computer Engineering', 20),
(21, 'elliot@priest.com', '1234abcd', 'Elliot Priest', 'Male', 'Single', 'Harvard Medical School', 'Fifth', 'Biology', 21),
(22, 'rebecca@powers.com', '1234abcd', 'Rebecca Powers', 'Female', 'In a relationship', 'Emmanuel College', 'First', 'Communications', 22),
(23, 'richard@jamerson.com', '1234abcd', 'Richard Jamerson', 'Male', 'Single', 'Suffolk University', 'Third', 'Fashion Design', 23),
(24, 'tara@thompson.com', '1234abcd', 'Tara Thompson', 'Female', 'In a domestic partnership', 'UMASS Medical School', 'Fourth', 'Dentistry', 24),
(25, 'aaron@james.com', '1234abcd', 'Aaron James', 'Male', 'Single', 'Northeastern University', 'Fifth', 'Computer Engineering', 25);
