/**
 * @file Angular dashboard component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Preference } from '../../models/preference';
import { User } from '../../models/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: User; // Current user
  preferences: Preference; // Current user preferences
  roommates: User[]; // Working list used to drive the results
  lastPressedButton: String; // Keeps track of the active button pressed
  petCheck: Boolean; // State of "Prefers pets"
  utilitiesCheck: Boolean; // State of "Wants utilities included"
  Math: any; // Made a class member for HTML injection (workaround)

  constructor(private authService: AuthService) {
    this.Math = Math; // Allows for use within HTML
  }

  ngOnInit() {
    // Grab the current user information
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user.user;
      this.preferences = profile.user.preferences;
    },
    err => {
      console.log(err);
      return false;
    });

    // Start by getting everyone in the database
    this.onClickGetAllRoommates();

    // Set these to false to initalize, but they have data binding to update
    this.petCheck = false;
    this.utilitiesCheck = false;
  }

  // When the user clicks the "All" button
  onClickGetAllRoommates(){
    // Get all of the roommates from the database
    this.authService.getAllRoommates().subscribe(results => {
      this.roommates = results;
    },
    err => {
      console.log(err);
      return false;
    });

    this.lastPressedButton = 'all'; // Keep track of the last button press
  }

  // When the user clicks the "People at [school]" button
  onClickGetSchoolmates(){
    this.authService.getSchoolmates().subscribe(results => {
      this.roommates = results;
    },
    err => {
      console.log(err);
      return false;
    });

    this.lastPressedButton = 'school';
  }

  // When the user clicks the "[major] Majors" button
  onClickGetSameMajors(){
    this.authService.getSameMajors().subscribe(results => {
      this.roommates = results;
    },
    err => {
      console.log(err);
      return false;
    });

    this.lastPressedButton = 'major';
  }

  // When the user clicks the "[year] Year Students" button
  onClickGetSameYears(){
    this.authService.getSameYears().subscribe(results => {
      this.roommates = results;
    },
    err => {
      console.log(err);
      return false;
    });

    this.lastPressedButton = 'year';
  }

  // When the user clicks the "Also Looking in [location]" button
  onClickGetSameLocation(){
    this.authService.getSameLocation().subscribe(results => {
      this.roommates = results;
    },
    err => {
      console.log(err);
      return false;
    });

    this.lastPressedButton = 'location';
  }

  // When the user clicks the "People with Similar Budgets" button
  onClickGetSimilarBudgets(){
    this.authService.getSimilarBudgets().subscribe(results => {
      this.roommates = results;
    },
    err => {
      console.log(err);
      return false;
    });

    this.lastPressedButton = 'budget';
  }

  // When the "Prefers pets" checkbox is changed (check/uncheck)
  // NOTE: We need to be mindful of the state of the other checkbox to ensure
  //       everything displays as expected, hence why that state is also passed
  //       into the service function
  onClickPreferPets(){
    // Check the state of the checkbox
    if(this.petCheck){
      this.authService.getPreferPets(this.lastPressedButton,
         this.utilitiesCheck).subscribe(results => {
        this.roommates = results;
      },
      err => {
        console.log(err);
        return false;
      });
    }
    // Box was not checked
    else{
      this.onClickClearPets();
    }
  }

  // Run when the "Prefers pets" checkbox is unchecked or the
  // "Clear pets filter" button is clicked
  onClickClearPets(){
    this.petCheck = false; // In case the button was pressed

    // Check if the utilities box is checked
    if(this.utilitiesCheck){
      this.onClickPreferUtilities(); // This will take care of everything
    }
    else{
      // Check what the active filter button is and rerun that query
      switch(this.lastPressedButton){
        case 'all':
          this.onClickGetAllRoommates();
          break;
        case 'school':
          this.onClickGetSchoolmates();
          break;
        case 'major':
          this.onClickGetSameMajors();
          break;
        case 'year':
          this.onClickGetSameYears();
          break;
        case 'location':
          this.onClickGetSameLocation();
          break;
        case 'budget':
          this.onClickGetSimilarBudgets();
          break;
      }
    }
  }

  // When the "Wants utilities included" checkbox is changed (check/uncheck)
  onClickPreferUtilities(){
    if(this.utilitiesCheck){
      this.authService.getPreferUtilities(this.lastPressedButton,
         this.petCheck).subscribe(results => {
        this.roommates = results;
      },
      err => {
        console.log(err);
        return false;
      });
    }
    else{
      this.onClickClearUtilities();
    }
  }

  onClickClearUtilities(){
    this.utilitiesCheck = false;

    // This time, consult the "Prefers pets" checkbox
    if(this.petCheck){
      this.onClickPreferPets();
    }
    else{
      switch(this.lastPressedButton){
        case 'all':
          this.onClickGetAllRoommates();
          break;
        case 'school':
          this.onClickGetSchoolmates();
          break;
        case 'major':
          this.onClickGetSameMajors();
          break;
        case 'year':
          this.onClickGetSameYears();
          break;
        case 'location':
          this.onClickGetSameLocation();
          break;
        case 'budget':
          this.onClickGetSimilarBudgets();
          break;
      }
    }
  }

}
