/**
 * @file Angular register component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { School } from '../../models/school';
import { User } from '../../models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [User]
})
export class RegisterComponent implements OnInit {
  user: User; // The current user
  firstName: String; // First and last name are separated for form entry
  lastName: String;
  schools: School[]; // List of schools from the database

  constructor(
    private router: Router,
    private authService: AuthService,
    private dataService: DataService,
    private flashMessage: FlashMessagesService
  ) {
    // Instantiate a new User object for binding with form elements
    this.user = new User();
  }

  ngOnInit() {
    // Initialize some values for sake of having the form appear correctly
    this.user.gender = "Male";
    this.user.relationshipStatus = "Single";
    this.user.school = "No school selected..."; // Placeholder value

    // Grab all of the schools from the DB
    this.dataService.getAllSchools().subscribe(results => {
      this.schools = results;
    },
    err => {
      console.log(err);
      return false;
    });
  }

  // New user registration procedure, called by clicking the "Register" button
  onRegisterSubmit(){
    // Concatenate the final first and last names
    this.user.fullName = this.firstName + " " + this.lastName;

    // Register the user (communicate with the backend)
    this.authService.registerUser(this.user).subscribe(data => {
      if(data.success){
        this.flashMessage.show('You are now registered and can log in.',
         {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/login']);
      }
      else{
        this.flashMessage.show('Something went wrong. Please try again.',
         {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/register']);
      }
    });
  }
}
