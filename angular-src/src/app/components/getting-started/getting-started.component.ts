/**
 * @file Angular getting started component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.component.html',
  styleUrls: ['./getting-started.component.css']
})
export class GettingStartedComponent implements OnInit {
  constructor(private authService: AuthService) { }

  ngOnInit() { }

}
