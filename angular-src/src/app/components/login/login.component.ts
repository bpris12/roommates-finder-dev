/**
 * @file Angular login component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // Email and password used for logging in
  email: String;
  password: String;

  constructor(
    private router: Router,
    private authService: AuthService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  // Login procedure started by clicking the "Login" button
  onLoginSubmit(){
    // Construct a partial user object to send with the POST request
    const user = {
      email: this.email,
      password: this.password
    };

    // Authenticate the user
    this.authService.authenticateUser(user).subscribe(data => {
      // If successful, store the token and user object locally, show a
      // successful flash message, and go to the profile page
      if(data.success){
        this.authService.storeUserData(data.token);
        this.flashMessage.show('You are now logged in.',
                                {cssClass: 'alert-success', timeout: 4000});
        this.router.navigate(['/profile']);
      }
      // Upon failure, show a failure flash message
      else{
        this.flashMessage.show(data.msg,
                                {cssClass: 'alert-danger', timeout: 2000});
        this.router.navigate(['/login']);
      }
    });
  }

}
