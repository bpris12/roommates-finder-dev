/**
 * @file Angular profile component.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Neighborhood } from '../../models/neighborhood';
import { Preference } from '../../models/preference';
import { School } from '../../models/school';
import { User } from '../../models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [User]
})
export class ProfileComponent implements OnInit {
  user: User; // The current user
  updatedUser: User; // Any profile changes need to be kept separate
  preferences: Preference; // The current user preferences
  schools: School[]; // List of schools from the database
  neighborhoods: Neighborhood[]; // List of neighborhoods from the database
  touchedSliders: Boolean; // Trigger for showing/hiding some HTML elements

  constructor(
    private router: Router,
    private authService: AuthService,
    private dataService: DataService,
    private flashMessage: FlashMessagesService
  ){
    // Initialize the updated user with dummy values
    // NOTE: This is okay, as we will only be polling for values of interest,
    //       not the whole object, when we try to propagate changes
    this.updatedUser = new User();
    this.updatedUser.fillInPlaceholders();
    this.touchedSliders = false;
  }

  ngOnInit() {
    // Grab the user's information
    this.loadProfileInfo();

    // Grab all of the schools from the DB
    this.dataService.getAllSchools().subscribe(results => {
      this.schools = results;
    },
    err => {
      console.log(err);
      return false;
    });

    // Grab all of the neighborhoods from the DB
    this.dataService.getAllNeighborhoods().subscribe(results => {
      this.neighborhoods = results;
    },
    err => {
      console.log(err);
      return false;
    });
  }

  // Get the profile information to be displayed on the page
  loadProfileInfo() {
    this.authService.getProfile().subscribe(profile => {
      // NOTE: "user" is just a name from the Passport packet coming from the
      //       backend, hence why this looks a little strange...
      this.user = profile.user.user;
      this.preferences = profile.user.preferences; // These are included too
    },
    err => {
      console.log(err);
      return false;
    });
  }

  // Called when the user does not have any linked preferences and needs them
  initPreferences(){
    // Create a holder object for new preferences
    let blankPrefs = new Preference();

    // Fill in default values to start - the user will adjust them
    blankPrefs.initDefaults();

    // Send the default object to the backend to create a primary key and
    // apply the foreign key constraint to the user profile
    this.authService.createPreferences(this.user, blankPrefs).subscribe(data =>
     {
      if(data.success){
        this.flashMessage.show('Your preferences profile has been created!',
         {cssClass: 'alert-success', timeout: 3000});
        this.loadProfileInfo();
      }
      else{
        this.flashMessage.show('Something went wrong. Please try again.',
         {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/profile']);
      }
    });
  }

  // Function for controlling the show/hide of certain HTML buttons
  onChangeTouchSlider(){
    this.touchedSliders = true;

    // Do a little data validation on the budget values
    if((this.preferences.minBudget == undefined) || (this.preferences.minBudget < 0)) {
      this.preferences.minBudget = 0;
    }
    if((this.preferences.maxBudget == undefined) || (this.preferences.maxBudget > 9999)) {
      this.preferences.maxBudget = 9999;
    }
  }

  // Copy the user info into a new object for manipulation
  onClickEdit(){
    this.updatedUser = JSON.parse(JSON.stringify(this.user)); // Workaround
  }

  // Run when the user clicks the "Save changes" button while editing email
  onClickUpdateEmail(){
    // Check if the email has really changed
    if((this.updatedUser.email != undefined) &&
       (this.updatedUser.email != this.user.email)) {
      // Update the newly entered user email (communicate with the backend)
      this.authService.updateUserEmail(this.updatedUser).subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your email has been updated.',
           {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
           {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
      });
    }
  }

  // Run when the user clicks the "Save changes" button while editing gender
  onClickUpdateGender(){
    if((this.updatedUser.gender != undefined) &&
       (this.updatedUser.gender != this.user.gender)) {
      this.authService.updateUserGender(this.updatedUser).subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your gender has been updated.',
           {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
           {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
      });
    }
  }

  // Run when the user clicks the "Save changes" button while editing
  // relationship status
  onClickUpdateRelationshipStatus(){
    if((this.updatedUser.relationshipStatus != undefined) &&
       (this.updatedUser.relationshipStatus != this.user.relationshipStatus)) {
      this.authService.updateUserRelationshipStatus(this.updatedUser)
        .subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your relationship status has been updated.',
           {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
           {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
      });
    }
  }

  // Run when the user clicks the "Save changes" button while editing school
  onClickUpdateSchool(){
    if((this.updatedUser.school != undefined) &&
       (this.updatedUser.school != this.user.school)) {
      this.authService.updateUserSchool(this.updatedUser).subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your school has been updated.',
           {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
           {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
      });
    }
  }

  // Run when the user clicks the "Save changes" button while editing school year
  onClickUpdateYearInSchool(){
    if((this.updatedUser.yearInSchool != undefined) &&
       (this.updatedUser.yearInSchool != this.user.yearInSchool)) {
      this.authService.updateUserYearInSchool(this.updatedUser).
      subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your year in school has been updated.',
           {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
           {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
      });
    }
  }

  // Run when the user clicks the "Save changes" button while editing major
  onClickUpdateMajor(){
    if((this.updatedUser.major != undefined) &&
       (this.updatedUser.major != this.user.major)) {
      this.authService.updateUserMajor(this.updatedUser).subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your major has been updated.',
          {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
          {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
      });
    }
  }

  // Run when the user clicks the "Update Preferences" button
  // NOTE: This function will always pass the entire updated preferences
  //       object, but this is alright because 1) MySQL will handle writes
  //       of the same items and 2) our previously-initialized blank object
  //       with dummy values has now been populated to be a mirror of the
  //       subscribed master object we are referencing
  onClickUpdatePreferences(){
    if(this.preferences != undefined) {
      this.authService.updatePreferences(this.preferences).subscribe(data => {
        if(data.success){
          this.flashMessage.show('Your preferences have been updated.',
           {cssClass: 'alert-success', timeout: 3000});
          this.loadProfileInfo();
          this.router.navigate(['/profile']);
          this.touchedSliders = false;
        }
        else{
          this.flashMessage.show('Something went wrong. Please try again.',
           {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/profile']);
        }
        window.scroll(0,0); // Scroll to the top to see the flash message
      });
    }
  }

  // Run when the user clicks the "Delete" button in the delete account modal
  onClickDelete(){
    // NOTE: We only need to pass the user object; the backend will find any
    //       associated preferences and delete those as well for a clean delete
    this.authService.deleteUser(this.user).subscribe(data => {
      if(data.success){
        this.authService.logout(); // Still need to log out to clear the service
        this.flashMessage.show('Your account has been deleted.',
        {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/']);
      }
      else{
        this.flashMessage.show('Something went wrong. Please try again.',
        {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/profile']);
      }
    });
  }

}
