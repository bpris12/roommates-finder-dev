/**
 * @file Frontend neighborhood model class that matches the backend schema.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

export class Neighborhood {
  neighborhoodName: String;
  description: String;
  zipCode: String;
  publicTransportation: Boolean;
  typeofAccommodations: String;
};
