/**
 * @file Frontend user model class that matches the backend schema.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

export class User {
  userid: Number; // Taken care of my MySQL upon insert
  email: String;
  fullName: String;
  major: String;
  gender: String;
  school: String;
  yearInSchool: String;
  relationshipStatus: String;
  preferenceid: Number;

  // Populate the object with blank values to supress rendering errors
  fillInPlaceholders(){
    this.userid = null;
    this.email = "";
    this.fullName = "";
    this.major = "";
    this.gender = "";
    this.yearInSchool = "";
    this.relationshipStatus = "";
    this.preferenceid = null;
  }
}
