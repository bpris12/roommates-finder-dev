/**
 * @file Frontend preference model class that matches the backend schema.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

export class Preference {
  preferenceid: Number; // Taken care of my MySQL upon insert
  party: Number;
  sleep: Number;
  noiseLevel: Number;
  petPreference: Boolean;
  firstChoiceNeighborhood: String;
  utilitiesIncluded: Boolean;
  maxNumofRoommates: Number;
  minBudget: Number;
  maxBudget: Number;

  // Fill in default values
  // NOTE: This is not strictly necessary, as we could rely on the DB to take
  //       care of defaults, but this lessens the chance of an error.
  initDefaults(){
    this.party = 0;
    this.sleep = 0;
    this.noiseLevel = 0;
    this.petPreference = true;
    this.firstChoiceNeighborhood = 'No neighborhood selected...';
    this.utilitiesIncluded = true;
    this.maxNumofRoommates = 1;
    this.minBudget = 0;
    this.maxBudget = 9999;
  }

}
