/**
 * @file Frontend school model class that matches the backend schema.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

export class School {
  schoolName: String;
  neighborhood: String;
  hasHousing: Boolean;
};
