/**
 * @file Angular data service.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { baseUrl } from '../../config/environment';

@Injectable()
export class DataService {

  constructor(private http: Http) { }

  // Get all of the neighborhoods from the backend
  getAllNeighborhoods(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getneighborhoods', {headers: headers})
      .map(res => res.json());
  }

  // Get all of the schools from the backend
  getAllSchools(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getschools', {headers: headers})
      .map(res => res.json());
  }
}
