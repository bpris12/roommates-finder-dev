/**
 * @file Angular authorization service.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

import { baseUrl } from '../../config/environment';

@Injectable()
export class AuthService {
  authToken: any;

  constructor(private http: Http) { }

  // Attempt to register the user with the backend
  registerUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    // Make the POST request
    return this.http.post(baseUrl + '/users/register', user, {headers: headers})
      .map(res => res.json());
  }

  // Attempt to authenticate the user with the backend
  authenticateUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(baseUrl + '/users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get the user profile information from the backend
  getProfile(){
    let headers = new Headers();
    this.loadToken(); // Grab the authorization token
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/users/profile', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to create and link the preferences profile with the backend (requires authentication)
  createPreferences(user, preferences){
    // Here, we need to send both the user and the new preferences
    let payload = Object.assign({}, user, preferences);
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.post(baseUrl + '/users/createprefs', payload, {headers: headers})
      .map(res => res.json());
  }

  // Attempt to update the user email in the database
  updateUserEmail(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updateemail', user, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to update the user gender in the database
  updateUserGender(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updategender', user, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to update the user relationship status in the database
  updateUserRelationshipStatus(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updaterelationship', user, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to update the user relationship status in the database
  updateUserSchool(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updateschool', user, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to update the user year in school in the database
  updateUserYearInSchool(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updateyear', user, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to update the user major in the database
  updateUserMajor(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updatemajor', user, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to update the user preferences in the database
  updatePreferences(preferences){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/updateprefs', preferences, {headers: headers})
        .map(res => res.json());
  }

  // Attempt to delete the user from the database
  deleteUser(user){
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization', this.authToken); // Protected request
      headers.append('Content-Type', 'application/json');
      return this.http.post(baseUrl + '/users/remove', user, {headers: headers})
        .map(res => res.json());
  }

  // Store the token in local storage
  storeUserData(token){
    localStorage.setItem('id_token', token);
    this.authToken = token;
  }

  // Load the token from local storage
  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  // Return true if the user is logged in, false otherwise
  loggedIn(){
    return tokenNotExpired('id_token');
  }

  // Clear the token, user object, and local storage
  logout(){
    this.authToken = null;
    localStorage.clear();
  }

  // NOTE: The rest of the functions are more suited to exist in the data
  //       service, but protected requests need to remain here to share the
  //       auth token

  // Attempt to get the list of roommates from the backend
  getAllRoommates(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getroommates', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get roommates at the same school
  getSchoolmates(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getschoolmates', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get roommates in the same major
  getSameMajors(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getsamemajors', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get roommates at the same school
  getSameYears(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getsameyears', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get roommates preferring the same location
  getSameLocation(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getsameloc', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get roommates with similar budgets
  getSimilarBudgets(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getsimilarbudgets', {headers: headers})
      .map(res => res.json());
  }

  // Attempt to get roommates who prefer pets (pass in the last query result)
  getPreferPets(lastPressedButton, utilitiesCheck){
    let headers = new Headers();
    let search = new URLSearchParams();
    search.set('appliedFilter', lastPressedButton);
    search.set('utilitiesCheck', utilitiesCheck);
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getpreferpets', {headers: headers, search: search})
      .map(res => res.json());
  }

  // Attempt to get roommates who prefer utilities
  getPreferUtilities(lastPressedButton, petCheck){
    let headers = new Headers();
    let search = new URLSearchParams(); // For appending queries
    search.set('appliedFilter', lastPressedButton);
    search.set('petCheck', petCheck);
    this.loadToken();
    headers.append('Authorization', this.authToken); // Protected request
    headers.append('Content-Type', 'application/json');
    return this.http.get(baseUrl + '/data/getpreferutils', {headers: headers, search: search})
      .map(res => res.json());
  }
}
