/**
 * @file Angular authorization guard.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate{
  constructor(private router: Router, private authService: AuthService){

  }

  // Verifies that the user is logged in to complete the request
  canActivate(){
    // If the user is logged in, return true
    if(this.authService.loggedIn()){
      return true;
    }
    // If the user is not logged in, return false and redirect to login
    else{
      this.router.navigate(['/login']);
      return false;
    }
  }
}
