/**
 * @file Environment configuration file for frontend.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

// Set this to true if you are planning on running the local dev server,
// or false for deployment.
const DEVELOPMENT_MODE = false;

export var baseUrl:String = DEVELOPMENT_MODE ? "http://localhost:3000" : "";
