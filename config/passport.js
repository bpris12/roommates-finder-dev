/**
 * @file Passport configuration file containing the strategy.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const db = require('../config/db');

// The token is contained in the HTTP authorization header, so we need to
// extract the payload and then query the database to see if we got a match
module.exports = function(passport){
  // Construct the options object for the strategy
  let opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
  opts.secretOrKey = db.secret; // From db.js

  // Create the strategy with the above options
  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    // Query the database for the user and send it back if found
    let sql = "SELECT * FROM users WHERE userid='"+jwt_payload.userid+"'";
    db.query(sql, (err, result) => {
      // Return failure if there was an error
      if(err){
        return done(err, false);
      }

      // Looks okay, so construct the user object
      let userObject = Object.assign({}, result[0]);

      // Return success/failure depending on if we got a valid user back
      if(result != undefined && result.length != 0){
        // If the user has a preference linked, grab that too
        if(userObject.preferenceid != undefined) {
          let prefQuery = "SELECT * FROM preferences WHERE \
            preferenceid='"+userObject.preferenceid+"'"
          db.query(prefQuery, (err, prefResult) => {
            if(err) {
              return done(err, false); // There was supposed to be a preference
            }
            else{
              // Everything is good, so construct a combined object and send it
              let prefObject = Object.assign({}, prefResult[0]);
              let final = {user: userObject, preferences: prefObject};
              return done(null, final);
            }
          });
        }
        else{
          // No preferences are linked to this user
          let prefObject = {}; // Still need a placeholder to prevent errors
          let final = {user: userObject, preferences: prefObject};
          return done(null, final);
        }
      }
      else {
        return done(null, false); // The user was not in the database
      }
    });
  }));
}
