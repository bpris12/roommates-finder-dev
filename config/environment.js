/**
 * @file Environment configuration file for backend.
 * @author Ben Prisby <bdprisby@gmail.com>
 */

// Set this to true if you are planning on running the local dev server,
// or false for deployment.
const DEVELOPMENT_MODE = false;

module.exports.port = DEVELOPMENT_MODE ? 3000 : (process.env.PORT || 8080);
